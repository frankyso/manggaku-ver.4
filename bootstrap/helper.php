<?php 
	function humanDate($time)
	{
		$time = date("Y-m-d 00:00:00",strtotime($time));

		$Now = new Carbon\Carbon;
		$Time = new Carbon\Carbon;
		$Time = $Time->createFromTimeStamp(strtotime($time));

		if ($Time->diffInDays($Now, false) == 1) {
			return "Kemarin";
		}
		elseif ($Time->diffInDays($Now, false) == 0) {
			return "Hari Ini";
		}
		else
		{
			return $Time->format("d F Y");
		}
	}
?>