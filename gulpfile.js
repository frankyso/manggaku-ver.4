var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.styles([
        "/assets/bootstrap/css/bootstrap.css",
        "/assets/css/font-awesome.css",
        "/assets/css/themify-icons.css",
        "/assets/css/animate.min.css",
        "/assets/css/skins/palette.css",
        "/assets/css/fonts/font.css",
        "/assets/plugins/stepy/jquery.stepy.css",
        "/assets/css/main.css",
        "/assets/plugins/tooltipster-master/css/tooltipster.css",
        ],"public/css/main.css","resources");

    mix.styles([
        "/assets/bootstrap/css/bootstrap.css",
        "/assets/css/font-awesome.css",
        "/assets/css/themify-icons.css",
        "/assets/css/animate.min.css",
        "/assets/css/skins/palette.css",
        "/assets/css/fonts/font.css",
        "/assets/plugins/chosen/chosen.min.css",
        '/assets/plugins/imgareaselect/css/imgareaselect-animated.css',
        "/assets/css/admin.css",
        ],"public/css/admin.css","resources");

    mix.scripts([
        "/assets/plugins/jquery-1.11.1.min.js",
        "/assets/bootstrap/js/bootstrap.js",
        '/assets/plugins/jquery.slimscroll.min.js',
        '/assets/plugins/jquery.easing.min.js',
        '/assets/plugins/appear/jquery.appear.js',
        '/assets/plugins/jquery.placeholder.js',
        '/assets/plugins/fastclick.js',
        '/assets/plugins/jquery.blockUI.js',
        '/assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js',
        '/assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js',
        '/assets/plugins/jquery.sparkline.js',
        '/assets/plugins/flot/jquery.flot.js',
        '/assets/plugins/flot/jquery.flot.resize.js',
        '/assets/plugins/count-to/jquery.countTo.js',
        '/assets/plugins/chosen/chosen.jquery.min.js',
        '/assets/plugins/imgareaselect/scripts/jquery.imgareaselect.js',
        '/assets/js/dashboard.js',
        '/assets/js/offscreen.js',
        '/assets/js/main.js',
    ], 'public/js/admin.js', 'resources');


    mix.scripts([
    	"/assets/plugins/jquery-1.11.1.min.js",
        "/assets/bootstrap/js/bootstrap.js",
    	"/assets/plugins/jquery.slimscroll.min.js",
        "/assets/plugins/jquery.easing.min.js",
        "/assets/plugins/appear/jquery.appear.js",
        "/assets/plugins/jquery.placeholder.js",
        "/assets/plugins/fastclick.js",
        "/assets/plugins/jquery.blockUI.js",
        "/assets/plugins/tooltipster-master/js/jquery.tooltipster.min.js",
        "/assets/js/offscreen.js",
        "/assets/plugins/fastclick.js",
        "/assets/plugins/isotope/isotope.pkgd.min.js",
        "/assets/js/catalog.js",
        "/assets/plugins/stepy/jquery.stepy.js",
        "/assets/plugins/stepy/jquery.validate.min.js",
        "/assets/plugins/fuelux/wizard.js",
        "/assets/plugins/bootstrap-rating-input/bootstrap-rating-input.min.js",
        '/assets/js/fuckadblock.js',
        "/assets/js/app.js",
        "/assets/js/main.js",
    ], 'public/js/main.js', 'resources');




    mix.scripts([
        "/assets/plugins/jquery-1.11.1.min.js",
    ], 'public/js/jquery.js', 'resources');


    /*Copying Bootstrap Fonts*/
    elixir(function(mix) {
        mix.copy('resources/assets/bootstrap/fonts/', 'public/fonts/');
        mix.copy('resources/assets/fonts/', 'public/fonts/');
    });
});