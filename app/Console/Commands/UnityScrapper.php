<?php

namespace ManggakuUnity\Console\Commands;

use Illuminate\Console\Command;

use Storage;
use Carbon;

use ManggakuUnity\Meta;
use ManggakuUnity\Chapter;
use ManggakuUnity\GrabList;
use ManggakuUnity\Scrapper;
use ManggakuUnity\Http\CustomClass\MAutoupdate;
class UnityScrapper extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'unity:scrap';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scrapping File Manga';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $now                =   Carbon::now()->timestamp;
        $updatelists        =   Scrapper::where('active','=','1');
        $updatelists->where('nextCheck','<',$now);
        $updatelists->orderBy('lastCheck','ASC');
        if ($updatelists->count()>0) {
            $updatelist = $updatelists->first();
            $updatelist->lastCheck = $now;
            $updatelist->save();

            $manga      = $updatelist->manga;
            $chapter    = $manga->getMaxChapter() + 1;
            $this->info('Updating Manga '.$manga->id."-".$manga->name." Chapter ".$chapter);
            $update     = new MAutoupdate($manga->id, $chapter);
            $result     = $update->execute();
            if ($result['status']) {
                $chapterNew = new Chapter;
                $chapterNew->chapter   = $chapter;
                $chapterNew->id_manga  = $manga->id;
                $chapterNew->save();
                $chapterNew->publish();

                $updatelist->nextCheck = $now + $updatelist->interval;
                $updatelist->save();

                $this->info('Success Grabbing Manga '.$manga->name.' Chapter '.$chapter);
                exit;
            }
        }
        
        
        /*Mulai Mencari Dalam Grablist*/
        $grablist = GrabList::where('execute_at','<',Carbon::now())->orderBy('execute_at','asc')->first();
        if ($grablist==null) {
            echo "NOTHING I CAN GRAB FROM LISTS";
            exit;
        }
        $this->info('Updating Manga From Lists');
        $this->info('Manga '.$grablist->manga->name.' chapter '.$grablist->chapter);
        $update     = new MAutoupdate($grablist->id_manga, $grablist->chapter);
        if ($grablist->grab_type==1) {
            $result     = $update->execute();
        }
        else
        {
            $result     = $update->execute(false, $grablist->source_url);
        }
        
        if ($result['status']) {
            $chapterNew = new Chapter;
            $chapterNew->chapter   = $grablist->chapter;
            $chapterNew->id_manga  = $grablist->id_manga;
            $chapterNew->save();
            $chapterNew->publish();

            $this->info('Success Grabbing Manga');
            $grablist->delete();
            exit;
        }
    }
}
