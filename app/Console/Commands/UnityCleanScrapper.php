<?php

namespace ManggakuUnity\Console\Commands;

use Illuminate\Console\Command;

use Storage;
class UnityCleanScrapper extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'unity:cleanScrapper';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clean Scrapping Log';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $allFiles = Storage::disk('local')->allFiles('scrapingLogs');
        foreach ($allFiles as $key => $value) {
            Storage::disk('local')->delete($value);
        }
    }
}
