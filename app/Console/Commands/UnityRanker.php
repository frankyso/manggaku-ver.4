<?php

namespace ManggakuUnity\Console\Commands;

use Illuminate\Console\Command;

use Storage;
use Carbon;

use ManggakuUnity\Manga;
class UnityRanker extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'unity:rank';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculating Manga Ranking';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        foreach (Manga::get() as $key => $value) {
            $value->countPopularity();
        }
        $this->info('success');
    }
}
