<?php

namespace ManggakuUnity\Console\Commands;

use Illuminate\Console\Command;

use Storage;
class UnityMigrator extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'unity:migrator';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clean Scrapping Log';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /*Get All List On Server*/
        $mangas = Storage::disk('s3')->directories('mangas');
        $files = Storage::disk('s3')->allFiles($mangas[0]);
        foreach ($files as $key => $file) {
            $content = Storage::disk('s3')->get($file);
            Storage::put($file, $content);
            Storage::disk('s3')->delete($file);
        }
    }
}
