<?php

namespace ManggakuUnity\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\Inspire::class,
        Commands\UnityScrapper::class,
        Commands\UnityCleanScrapper::class,
        Commands\UnityRanker::class,
        Commands\UnityMigrator::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->everyMinute();

        $schedule->command('unity:scrap')
                 ->everyMinute()->withoutOverlapping();

        $schedule->command('unity:cleanScrap')
                 ->daily()->withoutOverlapping();

        $schedule->command('unity:rank')
                 ->daily()->withoutOverlapping();

        // $schedule->command('unity:migrator')
        //          ->everyMinute()->withoutOverlapping();
    }
}
