<?php

namespace ManggakuUnity;

use Mail;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Phoenix\EloquentMeta\MetaTrait;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword, MetaTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    public static function boot()
    {
        parent::boot();
        static::created(function($user)
        {
            $data=array('name'=>$user->name,'total'=>User::count());
            Mail::send('email.admin.newUser', $data, function ($message) {
                $message->to('frankyso.mail@gmail.com');
                $message->subject("User Baru Manggaku!");
            });
        });
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public function notification()
    {
        return $this->hasMany('ManggakuUnity\Notification','user_id');
    }

    public function readed()
    {
        return $this->hasMany('ManggakuUnity\Read','user_id');
    }

    public function recentlyRead()
    {
        return $this->readed()->groupBy('manga_id')->orderBy('created_at','desc')->get();
    }

    public function addPoint($point=1)
    {
        $currentStatsRead   = $this->getMeta('stats.read',0);
        $this->updateMeta('stats.read',$currentStatsRead+$point);

        $currentPoint       = $this->getMeta('point',0);
        $this->updateMeta('point',$currentPoint+$point);
    }
}
