<?php

namespace ManggakuUnity;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    protected $table = 'type';
}
