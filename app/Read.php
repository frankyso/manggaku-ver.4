<?php

namespace ManggakuUnity;

use Illuminate\Database\Eloquent\Model;

class Read extends Model
{
    protected $table = 'read';

    public function manga()
    {
    	return $this->belongsTo('ManggakuUnity\Manga','manga_id');
    }

    public function chapter()
    {
    	return $this->belongsTo('ManggakuUnity\Chapter','chapter_id');
    }

    public function lastChapter()
    {
    	return Static::where('manga_id','=',$this->manga_id)->orderBy('chapter_id','DESC')->first();
    }
}
