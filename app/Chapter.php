<?php

namespace ManggakuUnity;

use Illuminate\Database\Eloquent\Model;
use Carbon;
use Storage;
use Auth;

class Chapter extends Model
{
    protected $table = 'chapter';
    public static function boot()
    {
        parent::boot();
        static::created(function($chapter)
        {
            $manga = Manga::find($chapter->id_manga);
            $manga->view_last_update        = 0;
            $manga->lastChapter_created_at  = Carbon::now();
            $manga->save();

            foreach (User::where('roles','=','1')->get() as $key => $user) {
                $activity = new Notification;
                $activity->manga_id     =   $chapter->id_manga;
                $activity->chapter_id   =   $chapter->id;
                $activity->user_id      =   $user->id;
                $activity->eventName    =   "chapter.create";
                $activity->save();
            }
        });

        static::deleted(function($chapter)
        {
            Notification::where('chapter_id','=',$chapter->id)->delete();
        });

        static::saved(function($chapter)
        {
            $exists = Storage::disk('local')->exists('chapters/'.$chapter->id.".txt");
            if ($exists) {
                Storage::disk('local')->delete('chapters/'.$chapter->id.".txt");
            }
        });
    }

    public function manga()
    {
    	return $this->belongsTo('ManggakuUnity\Manga','id_manga');
    }

    public function scopePublished($query)
    {
       return $query->where('votes', '>', 100);
    }

    public function getPrevChapter()
    {
    	return static::where("chapter", '<', $this->chapter)->where('id_manga','=',$this->id_manga)->orderBy("chapter", 'DESC')->first();
    }

    public function getNextChapter()
    {
    	return static::where("chapter", '>', $this->chapter)->where('id_manga','=',$this->id_manga)->orderBy("chapter")->first();
    }

    public function getImages()
    {
        // $path = 'mangas/'.$this->manga->slug.'/'.$this->chapter;
        // $files = Storage::files($path);
        // if (count($files)==0) {
        //     /*Migrate from online to offline*/
        //     $online = Storage::disk('s3')->files($path);
        //     foreach ($online as $key => $value) {
        //         $content = Storage::disk('s3')->get($value);
        //         Storage::put($value, $content);
        //     }
        // }

        // $files = Storage::files($path);
        // natsort($files);
        // return array_values($files);


        // exit;


        // $mangaUploadURL = 'mangas/'.$manga->slug."/".$request->get('chapter')."/".$mangaImage->getClientOriginalName();


        $exists = Storage::disk('local')->exists('chapters/'.$this->id.".txt");
        if (!$exists) {
            $onlinePath = 'mangas/'.$this->manga->slug.'/'.$this->chapter;
            $files = Storage::allFiles($onlinePath);
            Storage::disk('local')->put('chapters/'.$this->id.".txt", json_encode($files));
        }

        $files = Storage::disk('local')->get('chapters/'.$this->id.".txt");
        $files = json_decode($files,false);
        natsort($files);
        return array_values($files);
    }

    public function publish()
    {
        $data['picture'] = $this->manga->getCover();
        $data['message'] = "[Update]
".$this->manga->name." - ".$this->chapter." Indonesia. 
Klik di: http://www.manggaku.com/manga/".$this->manga->slug."/
Selamat membaca, bantu SHARE ke temanmu.

-------------------------------------------
komik Bahasa Indonesia klik www.manggaku.com
Like Us On https://www.facebook.com/Manggaku/
Join Our Group: https://www.facebook.com/groups/manggaku/
";
        $data['access_token'] = env('FACEBOOK_PAGE_ACCESS_TOKEN');
        $data['link'] = "http://www.manggaku.com/manga/".$this->manga->slug;
        $post_url = 'https://graph.facebook.com/'.env('FACEBOOK_PAGE_ID').'/feed';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $post_url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $return = curl_exec($ch);
        curl_close($ch);
    }

    public function readed()
    {
        if (Auth::check()) {
            if (Static::read()->whereChapter_id($this->id)->count()) {
                return '<small><span class="fa fa-check text-success"></span>Read</small>';return '<small><span class="fa fa-check text-success"></span>Read</small>';
            }
        }
    }

    public function read()
    {
        return $this->hasMany('ManggakuUnity\Read','chapter_id');
    }
}
