<?php

namespace ManggakuUnity;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Phoenix\EloquentMeta\MetaTrait;
use Nicolaslopezj\Searchable\SearchableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Manga extends Model
{
    protected $table = 'manga';
    use MetaTrait;
    use SearchableTrait;
    use SoftDeletes;

    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchable = [
        'columns' => [
            'name' => 10,
            // 'users.last_name' => 10,
        ],
        // 'joins' => [
        //     'posts' => ['users.id','posts.user_id'],
        // ],
    ];

    public static function boot()
    {
        parent::boot();
        static::created(function($manga)
        {
            $scrapper = new Scrapper;
            $scrapper->id_manga     =   $manga->id;
            $scrapper->active       =   0;
            $scrapper->interval     =   0;
            $scrapper->lastcheck    =   0;
            $scrapper->nextcheck    =   0;
            $scrapper->source       =   "";
            $scrapper->save();
        });
    }

    public function grablist()
    {
        return $this->hasMany('ManggakuUnity\GrabList','id_manga','id');
    }

    public function chapter()
    {
    	return $this->hasMany('ManggakuUnity\Chapter','id_manga','id');
    }

    public function getAuthor()
    {
        return $this->belongsTo('ManggakuUnity\Creator','author');
    }

    public function getArtist()
    {
        return $this->belongsTo('ManggakuUnity\Creator','artist');
    }

    public function genre()
    {
        return $this->hasMany('ManggakuUnity\GenreRelation','id_manga');
    }

    public function status()
    {
        return $this->belongsTo('ManggakuUnity\Status','id_status');
    }

    public function type()
    {
        return $this->belongsTo('ManggakuUnity\Type','id_type');
    }

    public function scrapper()
    {
        return $this->hasOne('ManggakuUnity\Scrapper','id_manga');
    }

    public static function getLastUpdate($number=20)
    {
        $chapters = Manga::orderBy('lastChapter_created_at','desc')->take($number)->get();
    	foreach ($chapters as $chapter) {
    		$lastChapter = $chapter->chapter()->where('created_at','>=',Carbon::createFromTimestamp(strtotime($chapter->lastChapter_created_at))->setTime(0,0,0)->subDays(7))->orderBy("created_at",'desc')->get();
    		$lastChapterSingle = $lastChapter->first();
            if ($lastChapterSingle) {
                $results[] = array("id"=>$lastChapterSingle->manga->id,
                               "name"=>$lastChapterSingle->manga->name,
                               "slug"=>$lastChapterSingle->manga->slug,
                               "cover_path"=>$lastChapterSingle->manga->cover_path,
                               "id_status"=>$lastChapterSingle->manga->id_status,
                               "chapter"=>$lastChapter);
            }
    	}

    	return (object) $results;
    }

    public function getLastChapter()
    {
        $chapter = Chapter::where('id_manga',$this->id)->orderBy('created_at','desc')->take(1);

        if ($chapter->count()==0) {
            return false;
        }

        return $chapter->first()->chapter;
    }

    public function getMaxChapter()
    {
        $chapter = Chapter::where('id_manga',$this->id)->orderBy('chapter','desc')->take(1);

        if ($chapter->count()==0) {
            return false;
        }

        return $chapter->first()->chapter;
    }

    public function addView()
    {
        $this->view               += 1;
        $this->view_last_update   += 1;
        $this->save();
    }
    
    public function countPopularity()
    {
        $epoch  = Carbon::createFromTimestamp(strtotime(date("Y-m-1")));
        $now    = Carbon::now();

        /*Counting TD*/
        $epoch_seconds  = $epoch->day * 86400 + $epoch->second;
        $score          = $this->rate * $this->view_last_update;
        
        $order = log(max(abs($score), 1), 10);

        $this->popularity = round($order + $epoch_seconds, 7);
        $this->save();
    }

    public function setGenres($genres = array())
    {
        GenreRelation::where('id_manga','=',$this->id)->delete();
        foreach ($genres as $key => $genre) {
            $newGenre = new GenreRelation;
            $newGenre->id_manga     =   $this->id;
            $newGenre->id_genre     =   $genre;
            $newGenre->save();
        }
    }

    public function getCover($size = "original")
    {
        $this->getAllMeta();
        $url = config('app.manga_remote_url')."/".$this->getMeta('cover.'.$size);
        return $url;
    }

    public function getGenre()
    {
        $genreArray = array();
        $genre = $this->genre;
        foreach ($genre as $key => $value) {
            $genreArray[] = $value->genre->name;
        }

        return implode(", ", $genreArray);
    }
}
