<?php

namespace ManggakuUnity;

use Illuminate\Database\Eloquent\Model;

class Creator extends Model
{
    protected $table = 'creator';

    public $timestamps = false;
}
