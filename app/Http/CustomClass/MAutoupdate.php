<?php 
	namespace ManggakuUnity\Http\CustomClass;
	use ManggakuUnity\Manga;
	use ManggakuUnity\ImageBlackList;
	use Image;
	use League\Flysystem\Filesystem;
	use Storage;
	use Exception;

	class MAutoupdate
	{
		public $loop = true;
		public $startPage = 1;
		public $currentPage = 1;

		public $images = array();

		public $sourceChapter 	= "";
		public $sourceURL	 	= "";

		public $imagesMinWidth 	= 300;
		public $imagesMinHeight	= 400;

		public $LogName 		= "";


		function __construct($mangaID, $chapter)
		{
			$this->id 				= $mangaID;
			$this->sourceChapter 	= $chapter;

			$manga = Manga::find($this->id);
			if ($manga->id == null) {
				throw new Exception("CANNOT FIND ID MANGA", 1);
			}

			$CRONSouceURL = $manga->scrapper->source;
			if ($CRONSouceURL==null) {
				// throw new Exception("CANNOT FIND CRON SOUCE URL ID MANGA", 1);
			}
			$this->sourceURL	= $CRONSouceURL;

			/*Log Name*/
			$this->LogName 		= "scrapingLogs/".$this->id."-".$this->sourceChapter;

			/*Check Log If Available*/
			if (!Storage::disk('local')->has($this->LogName)) {
				Storage::disk('local')->put($this->LogName, 'START');
			}
			else
			{
				$localList = Storage::disk('local')->get($this->LogName);
				$localList = explode("\n", $localList);
				if ($localList[0]=="START") {
					$this->startPage = count($localList);
				}
				$this->images 	 = $localList;
				array_shift($this->images);
			}

		}

		public function execute($perPage=true, $sourceURL = ""){
			$manga = Manga::find($this->id);

			if ($perPage) {
				$result = $this->executePerPage();
			}
			else
			{
				$this->sourceURL = $sourceURL;
				$result = $this->executeSinglePage();
			}

			if ($result===false) {
				return ["status"=>false];
			}

			if (count($this->images)<=3) {
				return ["status"=>false];
			}

			if ($this->UploadManga()) {
				return ["status"=>true,"result"=>$this->images];	
			}
		}

		private function executePerPage()
		{
			$localList = Storage::disk('local')->get($this->LogName);
			$localList = explode("\n", $localList);
			if ($localList[0] == "FINISH") {
				return;
			}

			$loop = $this->loop;
			while ($loop = true) {
				$image 		= $this->grabImages($this->generateURLPage());
				if ($image===false) {
					return false;
				}
				if (count($image) == 0) {
					$localList = Storage::disk('local')->get($this->LogName);
					$localList = explode("\n", $localList);
					$localList[0] = "FINISH";
					Storage::disk('local')->put($this->LogName, implode("\n", $localList));
					return;
				}
				else
				{
					$this->images[] = $image[0];
					Storage::disk('local')->append($this->LogName, $image[0]);
				}
				$this->currentPage += 1;
			}
		}

		private function executeSinglePage()
		{
			$localList = Storage::disk('local')->get($this->LogName);
			$localList = explode("\n", $localList);
			if ($localList[0] == "FINISH") {
				return;
			}

			$image 		= $this->grabImages($this->sourceURL);
			if ($image==false) {
				return false;
			}
			Storage::disk('local')->put($this->LogName, "FINISH\n".implode("\n", $image));
			$this->images 	= $image;
		}

		private function generateURLPage()
		{
			return $this->sourceURL."/".str_pad($this->sourceChapter, 3, '0', STR_PAD_LEFT)."/".$this->currentPage;
		}

		private function grabImages($url)
		{
			$imageList = array();
			try {
				$pages = new \Htmldom($url);
				if (count($pages->find('img'))==0) {
					return false;
				}

				foreach($pages->find('img') as $element)
				{
					$image = $this->purifyImageURL($element->src);
					$imageList[] = $image;
				}

				$blackListArray = $this->getBlacklistURL();
				$imageList = array_diff($imageList, $blackListArray);
				$imageList = $this->purifyMangaImages($imageList);
				
				return $imageList;
			} catch (Exception $e) {
				return $imageList;
			}
		}

		private function getBlacklistURL()
		{
			foreach (ImageBlackList::get() as $key => $value) {
				$blacklisted[] = $value->source;
			}

			return $blacklisted;
		}

		private function purifyImageURL($url)
		{
			/*Check if URL Using HTTP*/
			if (!str_contains($url, "https://")) {
				if(!str_contains($url, "http://"))
				{
					$host = parse_url($this->sourceURL);
					if (substr($url, 0, 1)=="/") {
						$resultURL = $host['scheme']."://".$host['host'].$url;
					}
					else
					{
						if (config('app.url_scrapper.'.str_replace(".", "(dot)", $host['host']))!=null) {
							$resultURL 		= config('app.url_scrapper.'.str_replace(".", "(dot)", $host['host']))."/".$url;
						}
						else
						{
							$urlExplode 	= explode("/", $host['path']);
							array_pop($urlExplode);
							$urlImplode 	= implode("/", $urlExplode);
							$resultURL 		= $host['scheme']."://".$host['host'].$urlImplode."/".$url;
						}
					}
				}
				else
				{
					$resultURL = $url;
				}
			}
			else
			{
				$resultURL = $url;
			}

			$resultURL = str_replace(" ", "%20", $resultURL);
			$resultURL = str_replace("+", "%2B", $resultURL);
			return $resultURL;
		}

		private function purifyMangaImages($array)
		{
			foreach ($array as $key => $value) {
				try {
					$image = Image::make($value);
					if ($image->width() >= $this->imagesMinWidth && $image->height() >= $this->imagesMinHeight) {
						$resultArray[] = $value;
						echo "Founded Manga Image - $value\n";
					}
					else
					{
						$blacklist = new ImageBlackList;
						$blacklist->source = $value;
						$blacklist->save();
					}
				} catch (\Intervention\Image\Exception\NotReadableException $e) {
					echo $e->getMessage();
					echo "<br>";
					$blacklist = new ImageBlackList;
					$blacklist->source = $value;
					$blacklist->save();
				}
			}

			return @$resultArray;
		}

		private function uploadManga()
		{
			$manga = Manga::find($this->id);
			$SisaUpload = $this->images;
			$number = 1;
			foreach ($this->images as $key => $value) {
				$name = $number.".".pathinfo($value, PATHINFO_EXTENSION);
				Storage::put('mangas/'.$manga->slug.'/'.$this->sourceChapter."/".$name, file_get_contents($value));
				array_shift($SisaUpload);
				Storage::disk('local')->put($this->LogName, "FINISH\n".implode("\n", $SisaUpload));
				$number++;
			}
			Storage::disk('local')->delete($this->LogName);

			return true;
		}
	}
?>