<?php

namespace ManggakuUnity\Http\Controllers;

use Illuminate\Http\Request;
use ManggakuUnity\Http\Requests;
use ManggakuUnity\Http\Controllers\Controller;
use ManggakuUnity\Http\CustomClass\MAutoupdate;

use Storage;
use Carbon;

use ManggakuUnity\Meta;
use ManggakuUnity\Chapter;
use ManggakuUnity\GrabList;
use ManggakuUnity\Scrapper;

class CronController extends Controller
{
    public function getIndex()
    {
        $now                =   Carbon::now()->timestamp;
        $updatelists        =   Scrapper::where('active','=','1');
        $updatelists->where('nextCheck','<',$now);
        $updatelists->orderBy('lastCheck','ASC');
        if ($updatelists->count()>0) {
            $updatelist = $updatelists->first();
            
                $manga      = $updatelist->manga;
                $chapter    = $manga->getLastChapter() + 1;                
                $update     = new MAutoupdate($manga->id, $chapter);
                $result     = $update->execute();
                if ($result['status']) {
                    $chapterNew = new Chapter;
                    $chapterNew->chapter   = $chapter;
                    $chapterNew->id_manga  = $manga->id;
                    $chapterNew->save();
                    exit;
                }
            
        }
    }

    public function getUploads3()
    {
    	$rootMangaDirectory = config('app.manga_local_path');

        $mangaDirectoryLists = scandir($rootMangaDirectory);
        $mangaDirectoryLists = array_diff($mangaDirectoryLists, array('.', '..','.DS_Store'));
        
        /*Get First Folder*/
        foreach ($mangaDirectoryLists as $key => $directory) {
            $chapterDirectory           =   $rootMangaDirectory."/".$directory;
            $chapterDirectoryLists      =   array_diff(scandir($chapterDirectory), array('.', '..','.DS_Store'));
            foreach ($chapterDirectoryLists as $key => $chapter) {
                $fileDirectory          = $chapterDirectory."/".$chapter;
                $fileDirectoryLists     =   array_diff(scandir($fileDirectory), array('.', '..','.DS_Store'));

                /*Time To Upload It*/
                foreach ($fileDirectoryLists as $key => $file) {
                    $onlinePath = 'mangas/'.$directory.'/'.$chapter."/".$file;
                    Storage::put($onlinePath, file_get_contents($fileDirectory.'/'.$file));
                    unlink($fileDirectory.'/'.$file);
                }
                
                rmdir($fileDirectory);
            }
            rmdir($chapterDirectory);
            exit;
        }
    }
}
