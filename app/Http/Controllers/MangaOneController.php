<?php

namespace ManggakuUnity\Http\Controllers;

use Illuminate\Http\Request;
use Storage;
use League\Flysystem\Filesystem;
use ManggakuUnity\Genre;
use ManggakuUnity\GenreRelation;
use ManggakuUnity\Manga;

use ManggakuUnity\Http\Requests;
use ManggakuUnity\Http\Controllers\Controller;

use ManggakuUnity\Read;
use Auth;

class MangaOneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex($slug, $chapter=null, $page=1)
    {
        $mangaModel = Manga::where('slug','=',$slug);
        if ($mangaModel->count()==0) {
            return redirect('/directory');
        }
        $mangaModel = $mangaModel->first();
        $chapterModel = $mangaModel->chapter()->where('chapter','=',$chapter);
        $adult = $mangaModel->genre()->whereHas('genre',function($query){
            $query->where('is_adult','=',1);
        })->count();
        

        if ($chapter==null || $chapterModel->count()==0) {
            return view('mangaDetail',["manga" => $mangaModel,
                                    'page_title'=>'Manga '.$mangaModel->name.' - Baca '.$mangaModel->name.' Online Gratis by Manggaku', 
                                    'page_keywords'=>"".$mangaModel->name.", manga ".$mangaModel->name.", baca manga ".$mangaModel->name.", manga ".$mangaModel->name." online, serial manga ".$mangaModel->name.", chapter manga ".$mangaModel->name."",
                                    'page_description'=>"manga ".$mangaModel->name." - baca manga ".$mangaModel->name." secara gratis, tanpa perlu mendownload ".$mangaModel->name,
                                    'page_og_title'=>'Manga '.$mangaModel->name,
                                    'page_og_image'=>$mangaModel->getCover(),
                                    'adult'=>$adult]);
        }
        
        $chapterModel   = $chapterModel->first();
        $pageIndex      = $page - 1;
        $files          = $chapterModel->getImages();

        if (count($files)==0) {
            return redirect('/manga/'.$slug);
        }

        $nextChapterModel = $chapterModel->getNextChapter();
        $prevChapterModel = $chapterModel->getPrevChapter();

        if ($nextChapterModel==null) {
            $nextChapterUrl  =   "/manga/$slug/";
        }
        else
        {
            $nextChapterUrl =   "/manga/$slug/".$nextChapterModel->chapter;
        }

        if ($prevChapterModel==null) {
            $prevChapterUrl  =   "/manga/$slug/";
        }
        else
        {
            $prevChapterUrl  =   "/manga/$slug/".$prevChapterModel->chapter;
        }

        $mangaModel->addView();
        
        /*Add Into Readed*/
        if (Auth::check()) {
            if ($page == count($files)) {
                $read = Auth::user()->readed()->where('chapter_id','=',$chapterModel->id);
                if ($read->count()==0) {
                    $newRead = new Read;
                    $newRead->user_id       = Auth::user()->id;
                    $newRead->chapter_id    = $chapterModel->id;
                    $newRead->manga_id    = $mangaModel->id;
                    $newRead->save();
                }
            }

            Auth::user()->addPoint();
        }

        return view('mangaOneRead',["manga" => $mangaModel,
                                "mangas"=>Manga::get(),
                                "chapter"=>$chapter,
                                "chapters"=>$mangaModel->chapter()->orderBy('chapter','desc')->get(),
                                "page"=>$page,
                                "pages"=>count($files),
                                "images"=>$files,
                                "nextPageURL"=>$nextChapterUrl,
                                "prevPageURL"=>$prevChapterUrl,
                                'page_title'=>$mangaModel->name." - Baca $mangaModel->name Online - Halaman $page", 
                                'page_keywords'=>"Baca $mangaModel->name $chapter, $mangaModel->name chapter $chapter, $mangaModel->name, manga $mangaModel->name, $mangaModel->name chapter $chapter halaman $page",
                                'page_description'=>"$mangaModel->name $chapter - Baca manga $mangaModel->name $chapter Halaman 1. secara gratis, tanpa perlu mendownload $mangaModel->name $chapter "]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
