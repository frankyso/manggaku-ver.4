<?php

namespace ManggakuUnity\Http\Controllers;

use Illuminate\Http\Request;

use ManggakuUnity\Http\Requests;
use ManggakuUnity\Http\Controllers\Controller;

use ManggakuUnity\Manga;
use ManggakuUnity\Creator;
use ManggakuUnity\Type;
use ManggakuUnity\Genre;
use ManggakuUnity\Status;
use ManggakuUnity\User;

use Auth;
use Validator;
use Image;
use Storage;
use Carbon;

class MangaAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $manga = Manga::orderBy('name','asc');
        $manga->where('name','like','%'.$request->get('q','').'%');

        return view('admin.mangaIndex',["mangas"    =>  $manga->paginate(30),'q'=>$request->get('q')]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $creators = Creator::orderBy('name','asc');
        $types = Type::orderBy('name','asc');
        $genres = Genre::orderBy('name','asc');
        $status = Status::orderBy('name','asc');
        return view('admin.mangaCreate',["creators"    =>  $creators,'types'=>$types,'genres'=>$genres,'statuses'=>$status]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'      =>  'required',
            'author'    =>  'required',
            'artist'    =>  'required',
            'released'  =>  'required|numeric',
            'plot'      =>  'required',
            'type'      =>  'required',
            'genre'     =>  'required',
            'status'    =>  'required',
            'coverURL'  =>  'url',
            'cover'     =>  'image',
        ]);

        if ($validator->fails()) {
            return redirect('prabu/manga/create')->withErrors($validator)->withInput();
        }

        $newManga = new Manga;
        $newManga->slug         =   str_slug($request->get('name'),'_');
        $newManga->name         =   $request->get('name');
        $newManga->author       =   $request->get('author');
        $newManga->artist       =   $request->get('artist');
        $newManga->released     =   $request->get('released');
        $newManga->plot         =   $request->get('plot');
        $newManga->id_type      =   $request->get('type');
        $newManga->id_status    =   $request->get('status');
        $newManga->save();

        /*cover*/
        // $newManga->status       =   $request->get('status');
        if ($request->hasFile('cover')) {
            $newManga->getAllMeta();
            $cover = $request->file('cover');
            $coverName = str_random(6)."_".$cover->getClientOriginalName();
        }
        else
        {
            $cover   = $request->get('coverURL');
            $coverName  = str_random(6)."_".basename($cover);   
        }

        /*Default Cover*/
        Storage::put('cover/original/'.$coverName, file_get_contents(($cover)));
        
        /*Medium Cover*/
        $img = Image::make(file_get_contents(($cover)));
        $img->fit(300,400);
        Storage::put('cover/large/'.$coverName, $img->stream()->__toString());

        /*Medium Cover*/
        $img = Image::make(file_get_contents(($cover)));
        $img->fit(100,150);
        Storage::put('cover/medium/'.$coverName, $img->stream()->__toString());

        /*small Cover*/
        $img = Image::make(file_get_contents(($cover)));
        $img->fit(50);
        Storage::put('cover/small/'.$coverName, $img->stream()->__toString());

        $newManga->updateMeta('cover.original', "cover/original/".$coverName);
        $newManga->updateMeta('cover.medium', "cover/medium/".$coverName);
        $newManga->updateMeta('cover.small', "cover/small/".$coverName);

        $newManga->save();
        $newManga->setGenres($request->get('genre'));

        return redirect(route('prabu.manga.show',$newManga->id));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $manga = Manga::find($id);
        if ($manga == null) {
            return redirect(route('prabu.manga.index'));
        }

        $manga->getAllMeta();
        $creators = Creator::orderBy('name','asc');
        $types = Type::orderBy('name','asc');
        $genres = Genre::orderBy('name','asc');
        $status = Status::orderBy('name','asc');

        return view('admin.mangaShow',['manga'=>$manga,"creators"    =>  $creators,'types'=>$types,'genres'=>$genres,'statuses'=>$status]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name'      =>  'required',
            'author'    =>  'required',
            'artist'    =>  'required',
            'released'  =>  'required|numeric',
            'plot'      =>  'required',
            'type'      =>  'required',
            'genre'     =>  'required',
            'status'    =>  'required',
            'coverURL'  =>  'url',
            'cover'     =>  'image',
        ]);

        if ($validator->fails()) {
            return redirect('prabu/manga/create')->withErrors($validator)->withInput();
        }

        $newManga = Manga::find($id);
        $newManga->name         =   $request->get('name');
        $newManga->author       =   $request->get('author');
        $newManga->artist       =   $request->get('artist');
        $newManga->released     =   $request->get('released');
        $newManga->plot         =   $request->get('plot');
        $newManga->id_type      =   $request->get('type');
        $newManga->id_status    =   $request->get('status');
        $newManga->save();

        /*cover*/
        if ($request->hasFile('cover')) {
            $newManga->getAllMeta();
            $cover = $request->file('cover');
            $coverName = str_random(6)."_".$cover->getClientOriginalName();
        }
        elseif($request->get('coverURL'))
        {
            $cover   = $request->get('coverURL');
            $coverName  = str_random(6)."_".basename($cover);   
        }
        else
        {
            $coverName="";
        }

        $newManga->scrapper->source     = $request->get('scrap');
        $newManga->scrapper->interval   = $request->get('scrap_interval',3)*86400;
        $newManga->scrapper->active     = $request->get('scrap_active',0);
        $newManga->scrapper->save();

        if ($coverName!=null || $coverName!="") {
            /*Default Cover*/
            Storage::put('cover/original/'.$coverName, file_get_contents(($cover)));

            $img = Image::make(file_get_contents(($cover)));
            $img->fit(300,400);
            Storage::put('cover/large/'.$coverName, $img->stream()->__toString());
            
            /*Medium Cover*/
            $img = Image::make(file_get_contents(($cover)));
            $img->fit(100,150);
            Storage::put('cover/medium/'.$coverName, $img->stream()->__toString());

            /*small Cover*/
            $img = Image::make(file_get_contents(($cover)));
            $img->fit(50);
            Storage::put('cover/small/'.$coverName, $img->stream()->__toString());

            $newManga->updateMeta('cover.original', "cover/original/".$coverName);
            $newManga->updateMeta('cover.medium', "cover/medium/".$coverName);
            $newManga->updateMeta('cover.small', "cover/small/".$coverName);

            $newManga->save();    
        }
        
        $newManga->setGenres($request->get('genre'));

        return redirect(route('prabu.manga.show',$newManga->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $manga = Manga::find($id);
        $manga->delete();
        return redirect(route('prabu.manga.index'));
    }
}
