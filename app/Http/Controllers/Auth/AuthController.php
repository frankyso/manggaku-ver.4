<?php

namespace ManggakuUnity\Http\Controllers\Auth;

use ManggakuUnity\User;
use Validator;
use Socialite;
use ManggakuUnity\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use Auth;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }


    public function redirectToProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function handleProviderCallback()
    {
        $facebook = Socialite::with('facebook')->user();
        $user = User::where('socialID','=',$facebook->getId());
        if ($user->count()==0) {
            $createUser = new User;
            $createUser->socialID   = $facebook->getId();
            $createUser->email      = $facebook->getEmail();
            $createUser->name       = $facebook->getName();
            $createUser->password   = bcrypt($facebook->getId());
            $createUser->avatar     = $facebook->getAvatar();
            $createUser->roles      = 10;
            $createUser->save();
            Auth::login($createUser);
            return redirect()->back();
        }

        Auth::login($user->first());
        return redirect()->back();
    }
}
