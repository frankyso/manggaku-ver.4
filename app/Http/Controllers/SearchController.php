<?php

namespace ManggakuUnity\Http\Controllers;

use Illuminate\Http\Request;

use ManggakuUnity\Http\Requests;
use ManggakuUnity\Http\Controllers\Controller;

use ManggakuUnity\Genre;
use ManggakuUnity\Status;
use ManggakuUnity\Manga;

class SearchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex(Request $request)
    {
        $manga = Manga::orderBy('popularity','DESC');

        $data['title'] = $request->get('title');
        if ($data['title']) {
            $manga->search($data['title'], null, true);
        }


        $data['author'] = $request->get('author');
        $data['illustrator'] = $request->get('illustrator');
        $data['genre'] = $request->get('genre',array());
        $data['releaseOrder'] = $request->get('releaseOrder');
        $data['release'] = $request->get('release');
        $data['status'] = $request->get('status');
        $data['searchResult'] = $manga->get();
        
        $data['genresList'] = Genre::orderBy('name','ASC')->get();
        $data['statusList'] = Status::orderBy('name','ASC')->get();

        return view('search',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
