<?php

namespace ManggakuUnity\Http\Controllers;

use Illuminate\Http\Request;

use ManggakuUnity\Http\Requests;
use ManggakuUnity\Http\Controllers\Controller;
use ManggakuUnity\Http\CustomClass\MAutoupdate;

use ManggakuUnity\Manga;
use ManggakuUnity\Chapter;
use ManggakuUnity\GrabList;

use Validator;
use Storage;
use Image;
use Carbon;

class ChapterAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id_manga'      =>  'required',
            'chapter'    =>  'required',
        ]);

        if ($validator->fails()) {
            return redirect('prabu/manga/'.$request->id_manga)->withErrors($validator)->withInput();
        }

        $manga = Manga::find($request->get('id_manga'));

        /*File Upload Handler*/
        if ($request->hasFile('file')) {
            foreach ($request->file('file') as $key => $mangaImage) {
                $mangaUploadURL = 'mangas/'.$manga->slug."/".$request->get('chapter')."/".$mangaImage->getClientOriginalName();
                $imageString = file_get_contents($mangaImage);

                Storage::put($mangaUploadURL, $imageString);
            }
        }
        elseif ($request->get('url')) {
            $ma = new MAutoupdate($manga->id, $request->get('chapter'));
            $ma->execute(false, $request->get('url'));
        }
        elseif ($request->get('fromDirectory')) {
            $ma = new MAutoupdate($manga->id, $request->get('chapter'));
            $ma->execute();
        }

        /*Saving Chapter*/
        $chapter = new Chapter;
        $chapter->chapter   = $request->input('chapter');
        $chapter->name      = $request->input('name','');
        $chapter->id_manga  = $request->input('id_manga');
        $chapter->save();
        $chapter->publish();

        return redirect(route('prabu.manga.show',$chapter->id_manga));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $chapter = Chapter::find($id);
        if ($chapter == null) {
            return redirect(route('prabu.manga.index'));
        }

        $directoryPath = 'mangas/'.$chapter->manga->slug.'/'.$chapter->chapter;
        $files = Storage::allFiles($directoryPath);

        return view('admin.chapterShow',['chapter'=>$chapter,'mangaLists'=>$files]);
    }

    public function batchUploadHandler(Request $request)
    {
        $manga = Manga::find($request->get('id_manga'));
        if ($request->get('batch',0)) {
            /*Looping*/
            $currentDate = Carbon::now();
            for ($i=$request->get('chapter'); $i <= $request->get('tochapter') ; $i++) { 
                $batch = new GrabList;
                $batch->source_url  =   $manga->scrapper->source;
                $batch->grab_type   =   1; //direktori
                $batch->id_manga    =   $manga->id;
                $batch->chapter     =   $i;
                $batch->execute_at  =   $currentDate;
                $batch->save();

                $currentDate->addDays($request->get('interval'));
            }
        }
        else
        {
            if ($request->get('fromDirectory',0)) {
                /*Dari Direktori*/
                $batch = new GrabList;
                $batch->source_url  =   $manga->scrapper->source;
                $batch->grab_type   =   1; //direktori
                $batch->id_manga    =   $manga->id;
                $batch->chapter     =   $request->get('chapter');
                $batch->execute_at  =   date("Y/m/d",strtotime($request->get('date')));
                $batch->save();
            }
            else
            {
                /*Dari URL*/
                $batch = new GrabList;
                $batch->source_url  =   $request->get('url');
                $batch->grab_type   =   2; //url
                $batch->id_manga    =   $manga->id;
                $batch->chapter     =   $request->get('chapter');
                $batch->execute_at  =   date("Y/m/d",strtotime($request->get('date')));
                $batch->save();
            }
        }

        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'chapter'    =>  'required',
        ]);

        if ($validator->fails()) {
            return redirect('prabu/chapter/'.$id)->withErrors($validator)->withInput();
        }

        $chapter = Chapter::find($id);

        if ($chapter == null) {
            return redirect(route('prabu.manga.index'));
        }

        $directoryPath = 'mangas/'.$chapter->manga->slug.'/'.$chapter->chapter;

        /*Hapus File Lamanya Dulu*/
        if ($request->get('deleteOldFile')) {
            Storage::deleteDirectory($directoryPath);
        }

        /*File Upload Handler*/
        if ($request->hasFile('file')) {
            foreach ($request->file('file') as $key => $mangaImage) {
                $mangaUploadURL = 'mangas/'.$chapter->manga->slug."/".$request->get('chapter')."/".$mangaImage->getClientOriginalName();
                $imageString = file_get_contents($mangaImage);

                Storage::put($mangaUploadURL, $imageString);
            }
        }
        elseif ($request->get('url')) {
            $ma = new MAutoupdate($chapter->manga->id, $request->get('chapter'));
            $ma->execute(false, $request->get('url'));
        }
        elseif ($request->get('fromDirectory')) {
            $ma = new MAutoupdate($chapter->manga->id, $request->get('chapter'));
            $ma->execute();
        }

        /*Saving Chapter*/
        $chapter->chapter   = $request->input('chapter');
        $chapter->name      = $request->input('name','');
        $chapter->save();

        return redirect(route('prabu.manga.show',$chapter->id_manga));
    }

    public function publish(Request $request, $id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $chapter = Chapter::find($id);
        $directoryPath = 'mangas/'.$chapter->manga->slug.'/'.$chapter->chapter;
        Storage::deleteDirectory($directoryPath);

        Chapter::find($id)->delete();
        return redirect(route('prabu.manga.show',$chapter->id_manga));
    }
}
