<?php

namespace ManggakuUnity\Http\Controllers;

use Illuminate\Http\Request;

use ManggakuUnity\Http\Requests;
use ManggakuUnity\Http\Controllers\Controller;

class SitemapController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sitemap = \App::make("sitemap");
        // $sitemap->setCache('laravel.sitemap', 60);
        // if ($sitemap->isCached())
        // {
            $sitemap->add(url('/'), '2012-08-25T20:10:00+02:00', '1.0', 'daily');
            $sitemap->add(url('partners'), '2012-08-26T12:30:00+02:00', '0.5', 'monthly');
            $sitemap->add(url('faq'), '2012-08-26T12:30:00+02:00', '0.5', 'monthly');
            $sitemap->add(url('join'), '2012-08-26T12:30:00+02:00', '0.5', 'monthly');
            $sitemap->add(url('directory'), '2012-08-26T12:30:00+02:00', '0.5', 'monthly');

            /*Eaching Per Manga*/
            foreach (\ManggakuUnity\Manga::get() as $key => $value) {
                $sitemap->add(url('/manga/'.$value->slug), $value->updated_at, '8.0', 'daily');
                foreach ($value->chapter()->get() as $key2 => $value2) {
                    $sitemap->add(url('/manga/'.$value->slug."/".$value2->chapter), $value2->updated_at, '6.0', 'monthly');
                }
            };
        // }
        $sitemap->store('sitemapindex','sitemap');
        return $sitemap->render('xml');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
