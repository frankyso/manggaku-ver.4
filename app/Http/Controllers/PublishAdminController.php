<?php

namespace ManggakuUnity\Http\Controllers;

use Illuminate\Http\Request;

use ManggakuUnity\Http\Requests;
use ManggakuUnity\Http\Controllers\Controller;

use ManggakuUnity\Manga;
use ManggakuUnity\Chapter;
use Image;
use Storage;

class PublishAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function postIndex(Request $request)
    {
        $image = Image::make($request->get('image'));
        $image->crop($request->get('w'),$request->get('h'),$request->get('x1'),$request->get('y1'));
        $image->insert('img/watermark.png', 'bottom-right', 10, 10);
        
        $filename = 'tmp/facebook/'.str_random(20).".png";
        Storage::disk('local')->put($filename, $image->encode('png')->stream()->__toString());
        $pathToFile=storage_path()."/app/".$filename;
        return response()->download($pathToFile);  

        // $data['picture'] = config('app.manga_remote_url')."/".$filename;
        // $data['message'] = $request->get('autopost');
        // $data['access_token'] = env('FACEBOOK_PAGE_ACCESS_TOKEN');
        // $data['link'] = "http://www.manggaku.com/";
        // $post_url = 'https://graph.facebook.com/'.env('FACEBOOK_PAGE_ID').'/feed';
        // $ch = curl_init();
        // curl_setopt($ch, CURLOPT_URL, $post_url);
        // curl_setopt($ch, CURLOPT_POST, 1);
        // curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        // $return = curl_exec($ch);
        // curl_close($ch);
        // return redirect(config('app.manga_remote_url')."/".$filename);
    }

    public function getIndex()
    {
        $session    = session()->get('publish');
        $chapter    = Chapter::find($session['id_chapter']);
        $template   = "[Update Terbaru]\nManga ".$chapter->manga->name." Chapter ".$chapter->chapter." Sudah terbit di www.manggaku.com \n\nSebarkan ke teman-temanmu!";
        return view('admin.publishCrop')->with('template',$template)->with('url',$session['imageToPublish']);
    }

    public function postSetimage(Request $request)
    {
        session(['publish' => $request->all()]);
        return redirect('prabu/publish');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
