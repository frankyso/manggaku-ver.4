<?php

namespace ManggakuUnity\Http\Controllers;

use Illuminate\Http\Request;

use Image; 
use ManggakuUnity\Genre;
use ManggakuUnity\GenreRelation;
use ManggakuUnity\Manga;

use ManggakuUnity\Http\Requests;
use ManggakuUnity\Http\Controllers\Controller;

class DirectoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function manga()
    {
        return view('manga');
    }

    public function getIndex()
    {
        $mangas = Manga::orderBy('popularity','desc')->paginate(16);

        return view('directory',["genres"    =>  Genre::orderBy('name','asc')->get(),'mangas'=>$mangas,
                                'page_title'=>'Directory Manga - baca manga by Manggaku']);
    }

    public function getGenre($genre)
    {
        $genreQuery = Genre::where('name','=',$genre);
        if ($genreQuery->count()==0) {
            return redirect('/directory');
        }

        $mangas = Manga::orderBy('popularity','desc');
        $mangasGenre = GenreRelation::where('id','!=',0)->where('id_genre','=',$genreQuery->first()->id);
        
            $mangas->where(function($subQuery) use ($mangasGenre){
                foreach ($mangasGenre->get() as $mangaGenre) {
                    $subQuery->orWhere('id','=',$mangaGenre->id_manga);
                }
            });
        

        return view('directory',["genres"    =>  Genre::orderBy('name','asc')->get(),'mangas'=>$mangas->paginate(16)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
