<?php

namespace ManggakuUnity\Http\Controllers;

use Illuminate\Http\Request;

use ManggakuUnity\Http\Requests;
use ManggakuUnity\Http\Controllers\Controller;

use ManggakuUnity\Manga;
use Storage;
use Image;

class IndexAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $manga = Manga::where('cover_path','!=','null');
        return view('admin.overview',["manga"=>$manga->first()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $manga = Manga::find($id);
        $manga->getAllMeta();
        $cover   = $request->get('coverURL');
        $coverName  = str_random(6)."_".basename($cover);   
        Storage::put('cover/original/'.$coverName, file_get_contents(($cover)));
        
        /*Medium Cover*/
        $img = Image::make(file_get_contents(($cover)));
        $img->fit(100,150);
        Storage::put('cover/medium/'.$coverName, $img->stream()->__toString());

        /*small Cover*/
        $img = Image::make(file_get_contents(($cover)));
        $img->fit(50);
        Storage::put('cover/small/'.$coverName, $img->stream()->__toString());

        $manga->updateMeta('cover.original', "cover/original/".$coverName);
        $manga->updateMeta('cover.medium', "cover/medium/".$coverName);
        $manga->updateMeta('cover.small', "cover/small/".$coverName);
        $manga->cover_path = null;
        $manga->save();
        return redirect(route('prabu.overview.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
