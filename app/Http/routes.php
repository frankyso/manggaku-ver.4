<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::resource('/', 'HomeController');
Route::controller('/manga/{slug}/{chapter?}/{page?}', 'MangaController');
Route::controller('/directory', 'DirectoryController');
Route::controller('/profile/{slug}', 'ProfileController');

Route::controller('/ajax', 'AjaxController');
Route::controller('/cron', 'CronController');

Route::get('auth/facebook', 'Auth\AuthController@redirectToProvider');
Route::get('auth/facebook/callback', 'Auth\AuthController@handleProviderCallback');
Route::get('/logout', function(){
	Auth::logout();	
	return redirect('/');
});


// Route::controller('/manga/{slug}/{chapter?}/{page?}', 'MangaController');
Route::controller('/search', 'SearchController');

Route::group(['middleware' => 'ManggakuUnity\Http\Middleware\AdminMiddleware','prefix' => 'prabu'], function()
{
    Route::resource('/overview', 'IndexAdminController');
    Route::resource('/user', 'UserAdminController');
    Route::resource('/manga', 'MangaAdminController');
    Route::resource('/chapter', 'ChapterAdminController');
    Route::post('/chapterbatch', 'ChapterAdminController@batchUploadHandler');
    Route::resource('/creator', 'CreatorAdminController');
    Route::controller('/publish', 'PublishAdminController');
    Route::resource('/redirect/{notificationID?}/', 'UltilityAdminController@redirect');
    // Route::resource('mn-admin/genre', 'GenreAdminController');
    // Route::resource('mn-admin/type', 'TypeAdminController');
    // Route::resource('mn-admin/status', 'StatusAdminController');
    // Route::resource('mn-admin/scrapper', 'ScrapperAdminController');
});

Route::get('distro', 'PageController@distro');
Route::get('partners', 'PageController@partners');
Route::get('faq', 'PageController@faq');
Route::get('join', 'PageController@join');
Route::resource('sitemap', 'SitemapController');
