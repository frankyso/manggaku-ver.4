<?php

namespace ManggakuUnity;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $table = 'notification';

    public static function scopeUnreaded($query)
    {
    	$query->where('readed','=','0');
    }

    public static function scopeReaded($query	)
    {
    	$query->where('readed','=','1');
    }

    public function manga()
    {
    	return $this->belongsTo('ManggakuUnity\Manga','manga_id');
    }

    public function chapter()
    {
    	return $this->belongsTo('ManggakuUnity\Chapter','chapter_id');
    }

    public function parseForAdmin()
    {
    	return view('admin.template.notification.chapterCreate')->with('notification',$this)->render();
    }

    public function generateURL()
    {
        if ($this->eventName=="chapter.create") {
            return "/prabu/chapter/".$this->chapter_id;
        }
    }
}
