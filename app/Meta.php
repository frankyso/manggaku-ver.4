<?php

namespace ManggakuUnity;

use Illuminate\Database\Eloquent\Model;

class Meta extends Model
{
    protected $table = 'meta';
    public $timestamps;

    public function manga()
    {
    	return $this->belongsTo('ManggakuUnity\Manga','metable_id');
    }
}
