<?php

namespace ManggakuUnity;

use Illuminate\Database\Eloquent\Model;

class GrabList extends Model
{
    protected $table = 'grablist';

    public function manga()
    {
    	return $this->belongsTo('ManggakuUnity\Manga','id_manga');
    }
}
