<?php

namespace ManggakuUnity\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->share('page_title', 'Manggaku - Baca Manga Online Gratis!');
        view()->share('page_keywords', 'baca manga, Baca Naruto, Manga Bahasa, Baca Online, Manga Indonesia, Manga Indo, Naruto');
        view()->share('page_description', 'Baca manga favoritmu! dengan ribuan koleksi manga untuk anda, yang update tiap hari. manga Naruto, manga Bleach, manga One Piece, manga Bleach, manga Claymore, manga Fairy Tail, dan masih banyak lagi.');

        view()->share('page_og_title', 'Baca Manga Online');
        view()->share('page_og_image', url('/'));
        view()->share('page_og_url', url('/'));
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
