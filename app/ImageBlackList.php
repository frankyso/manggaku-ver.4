<?php

namespace ManggakuUnity;

use Illuminate\Database\Eloquent\Model;

class ImageBlackList extends Model
{
    protected $table = 'image_blacklist';
    public $timestamps  = false;
}
