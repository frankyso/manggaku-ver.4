<?php

namespace ManggakuUnity;

use Illuminate\Database\Eloquent\Model;

class GenreRelation extends Model
{
    protected $table = 'genre_rel';
    public $timestamps  = false;

    public function genre()
    {
        return $this->belongsTo('ManggakuUnity\Genre','id_genre');
    }
}
