<?php

namespace ManggakuUnity;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    protected $table = 'rating';

    public static function boot()
    {
        parent::boot();

        static::saved(function($rate)
        {
        	$Rate = Rating::where('id_manga','=',$rate->id_manga);
        	$sumRate   	= $Rate->sum('rating');
        	$countRate 	= $Rate->count();

        	$manga = Manga::find($rate->id_manga);
        	$manga->rate 	=	floor($sumRate / $countRate);
        	$manga->save();
        	$manga->countPopularity();
        });
    }
}
