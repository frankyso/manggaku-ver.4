<?php

namespace ManggakuUnity;

use Illuminate\Database\Eloquent\Model;

class Scrapper extends Model
{
    protected $table = 'scrapper';

    public function manga()
    {
    	return $this->belongsTo('ManggakuUnity\Manga','id_manga');
    }
}
