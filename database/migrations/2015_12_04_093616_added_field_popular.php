<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddedFieldPopular extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('manga', function ($table) {
            $table->double('popularity', 15, 8)->after('rate');
            $table->integer('view_last_update')->after('view');
            $table->dateTime('lastChapter_created_at')->after('popularity');
        });

        Schema::create('grablist', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('source_url');
            $table->tinyInteger('grab_type');
            $table->integer('id_manga');
            $table->integer('chapter');
            $table->dateTime("execute_at");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('grablist');
    }
}
