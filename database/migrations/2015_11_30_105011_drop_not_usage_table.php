<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropNotUsageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::drop('scanlation_suggest');
        Schema::drop('scanlation');
        Schema::drop('genre_rel_suggest');
        Schema::drop('chapter_suggest');
        Schema::drop('approval');
        Schema::drop('suggest');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
