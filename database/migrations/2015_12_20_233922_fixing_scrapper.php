<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use ManggakuUnity\Manga;
use ManggakuUnity\Scrapper;

class FixingScrapper extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scrapper', function ($table) {
            $table->increments('id');
            $table->integer('id_manga');
            $table->tinyInteger('active');
            $table->bigInteger('interval');
            $table->bigInteger('lastcheck');
            $table->bigInteger('nextcheck');
            $table->text('source');
            $table->timestamps();
        });

        foreach (Manga::get() as $key => $value) {
            $scrapper = new Scrapper;
            $scrapper->id_manga     = $value->id;
            $scrapper->active       = $value->getMeta('scrap.active',0);
            $scrapper->lastcheck    = strtotime(date("Y-m-d H:i:s"));
            $scrapper->nextcheck    = $value->getMeta('scrap.nextCheck',0);
            $scrapper->source       = $value->getMeta('scrap.directory','');
            $scrapper->interval     = $value->getMeta('scrap.interval',0);
            $scrapper->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
