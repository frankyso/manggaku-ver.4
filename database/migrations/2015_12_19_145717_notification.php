<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Notification extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notification', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->integer('manga_id');
            $table->integer('chapter_id');
            $table->text('eventName');
            $table->tinyInteger('readed');
            $table->timestamps();
        });

        Schema::dropIfExists('approval');
        Schema::dropIfExists('autoupdate');
        Schema::dropIfExists('readed');
        Schema::dropIfExists('suggest');
        Schema::dropIfExists('posts_meta');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('notification');
    }
}
