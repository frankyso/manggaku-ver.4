<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use ManggakuUnity\Manga;
use ManggakuUnity\Chapter;

class Lastupdate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $manga = Manga::get();
        foreach ($manga as $key => $value) {
            $chapter = Chapter::where('id_manga',$value->id)->orderBy('created_at','desc')->take(1);
            if ($chapter->count()>0) {
                $value->lastChapter_created_at = $chapter->first()->created_at;
                $value->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
