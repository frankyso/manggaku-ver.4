$(document).ready(function()
{
	$(".cropimage").imgAreaSelect({ aspectRatio: '5:3', fadeSpeed: 200, onSelectChange: preview });
	function preview(img, selection) {
	    if (!selection.width || !selection.height)
	        return;
	    
	    var scaleX = 100 / selection.width;
	    var scaleY = 100 / selection.height;

	    $('#x1').val(selection.x1);
	    $('#y1').val(selection.y1);
	    $('#x2').val(selection.x2);
	    $('#y2').val(selection.y2);
	    $('#w').val(selection.width);
	    $('#h').val(selection.height);    
	}
});
