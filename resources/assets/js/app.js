var ManggakuRegister=function(){
	return{
		init:function(){
			$("#stepyRegister, #stepyLogin").stepy({
				backLabel:"Previous Step",
				nextLabel:"Next Step",
				errorImage:false,
				block:true,
				validate:false
			});

			$('#stepyRegister').validate({
				errorClass: "text-danger",
				errorElement: "span",
				rules: {
					password_confirm: {
						equalTo: "#password"
					},
					email_confirm: {
						equalTo: "#email"
					},
					username:{
						remote: "/user/check_username"
					}
				},
				messages:{
					username:{
						remote: "Username ini telah digunakan. silahkan pilih username lainnya"
					}
				}
			});
		}};
	}();
	$(function(){"use strict";ManggakuRegister.init();
});

	// Function called if AdBlock is detected
	function adBlockDetected() {
	    $("#tfadblock").modal({
		  keyboard: false,
		  backdrop: 'static',
		  show:true,
		});
	}

$(document).ready(function(){
	var loading_html = "<p class='text-white text-center mb25'><i class='fa fa-refresh fa-spin'></i> Loading...</p>";

	// Recommended audit because AdBlock lock the file 'fuckadblock.js' 
	// If the file is not called, the variable does not exist 'fuckAdBlock'
	// This means that AdBlock is present
	// if(typeof fuckAdBlock === 'undefined') {
	//     adBlockDetected();
	// } else {
	//     fuckAdBlock.onDetected(adBlockDetected);
	// }

	$("[data-manggaku='load']").each(function(){
		var widget_load = $(this);
		widget_load.append(loading_html);
		$.get("/ajax/widget/" + $(this).data("target"), function(data){
			widget_load.html(data);
		});
	});

	$("#rating").change(function(){
		$.post("/ajax/rate", { rate:$(this).val(), idm:$(this).data('manga')} ,function(data){
		    $("#rating-container").append(data);
		    $('.notice').delay(7000).hide('fast');
		});
	});

	$('.mn-tooltip').tooltipster({
	    content: 'Loading...',
	    updateAnimation: false,
	    maxWidth:500,
	    contentAsHTML: true,
	    touchDevices: false,
	    position:"bottom-left",
	    arrow: false,
	    functionBefore: function(origin, continueTooltip) {
	        continueTooltip();
	        
	        $.ajax({
	            type: 'POST',
	            url: '/ajax/mangatooltip?id='+$(this).data('id'),
	            success: function(data) {
	                origin.tooltipster('content', data).data('ajax', 'cached');
	            }
	        });
	    }
	});
});

// window.fbAsyncInit = function() {
//     FB.init({
//         appId:  '495377260495706',
//         status: true,
//         cookie: true,
//         xfbml:  true,
//         oauth: true
//     });

//     FB.Event.subscribe('comment.add', comment_create_callback);
//     FB.Event.subscribe('comment.remove', comment_remove_callback);
//     FB.Event.subscribe('auth.login', login_event);
//     FB.Event.subscribe('auth.logout', logout_event);
//     var comment_create_callback = function(response) {
//       console.log("comment_callback");
//       console.log(response);
//       $.post("/ajax/comment", { response:response, type:"create"} ,function(data){});
//     }

//     var comment_remove_callback = function(response) {
//       console.log("comment_callback");
//       console.log(response);
//       $.post("/ajax/comment", { response:response, type:"remove"} ,function(data){});
//     }

//     var login_event = function(response) {
//       console.log("login_event");
//       console.log(response.status);
//       console.log(response);
//       window.location.href='http://www.manggaku.com/auth/facebook';
//     }

//     var logout_event = function(response) {
//       console.log("logout_event");
//       console.log(response.status);
//       console.log(response);
//       window.location.href='http://www.manggaku.com/logout';
//     }

// };