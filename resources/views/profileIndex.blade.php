@include('_header')
	<h4 class="text-muted">Riwayat Bacaan</h4>
	<div class="row">
		@foreach(Auth::user()->recentlyRead() as $manga)
		<div class="col-md-3">
			<div class="panel panel-default">
				<div class="panel-body">
					<img src="{{$manga->manga->cover_path}}" alt="">
					<h5 class="text-center mb0">{{$manga->manga->name}}</h5>
				</div>
				<div class="panel-footer">
					<a href="/manga/{{$manga->manga->slug}}/{{@$manga->lastChapter()->chapter->@getNextChapter()->chapter}}" class="btn btn-block btn-primary">Lanjutkan Membaca</a>
				</div>
			</div>
		</div>
		@endforeach
	</div>
@include('_footer')