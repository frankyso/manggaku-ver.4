@include('_header')
<div class="row hidden-sm hidden-xs">
	<div class="col-md-12">
		<!--  ad tags Size: 728x90 ZoneId:1052096-->
		<script type="text/javascript" src="http://js.adstars.co.id/t/052/096/a1052096.js"></script>
	</div>
</div>
<div class="row">
	<div class="col-md-8">
		<div class="carousel slide mb25" data-ride="carousel" id="carousel-example-generic">
			<ol class="carousel-indicators">
				<!-- <li data-target="#carousel-example-generic" data-slide-to="0"></li> -->
				<!-- <li data-target="#carousel-example-generic" data-slide-to="1" class="active"></li> -->
			</ol>
			<div class="carousel-inner" role="listbox">
				<div class="item active">
					<img src="/img/banner2.jpg" alt="Banner Manggaku1">
				</div>
				<!-- <div class="item active">
					<img src="/img/banner3.jpg" alt="Banner Manggaku1">
				</div> -->


				<a href="#carousel-example-generic" class="left carousel-control" role="button" data-slide="prev">
					<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				</a>
				<a href="#carousel-example-generic" class="right carousel-control" role="button" data-slide="next">
					<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				</a>
			</div>
		</div>
		<div class="panel">
			<div class="panel-body">
				<ul class="list-manga-update">
					@foreach ($mangas as $manga)
						<li class="@if($manga['id_status']==1) ongoing @else completed @endif"><a class="mn-tooltip" data-id="{{$manga['id']}}" href="/manga/{{$manga['slug']}}">{{$manga['name']}}</a>
							<ul>
								<?php foreach ($manga['chapter'] as $chapter): ?>
									<li><a href="/manga/{{$manga['slug']}}/{{$chapter->chapter}}">{{$manga['name']}} {{$chapter->chapter}} {{$chapter->name}}</a> <em class="pull-right mr5">{{humanDate($chapter->created_at)}}</em></li>
								<?php endforeach ?>
							</ul>
						</li>
					@endforeach
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		{!!Form::open(array('url'=>'/search/','method'=>'GET'))!!}
		<div class="form-group mb20">
			<div class="input-group">
					<input name="title" placeholder="Ketikan Judul Manga" type="text" class="form-control">

					<div class="input-group-btn">
						<button type="submit" class="btn btn-default"><span class="fa fa-search"></span></button>
					</div>
			</div>
		</div>
		</form>
		<div class="clearfix"></div>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h1 class="panel-title"><a href="">Baca Manga</a></h1>
			</div>
			<img class="img img-responsive" src="img/banner/banner.gif" alt="banner manggaku">
			<div class="panel-body">
				<p>Baca komik terpopuler, bergabung dan lebih mendekatkan diri di komunitas online, berbagi pengetahuan tentang manga, dan Anda dapat juga membagikan hasil scanlation Anda secara online, semuanya dapat anda temukan secara gratis di <a href="">Manggaku</a></p>
				<p>
					<span class='st_facebook_vcount' displayText='Facebook'></span>
					<span class='st_twitter_vcount' displayText='Tweet'></span>
					<span class='st_sharethis_vcount' displayText='ShareThis'></span>
					<div class="clearfix"></div>
				</p>
			</div>
		</div>

		<div class="mb20">
			<!--  ad tags Size: 300x250 ZoneId:1052368-->
			<script type="text/javascript" src="http://js.adstars.co.id/t/052/368/a1052368.js"></script>
		</div>

		<div class="panel panel-danger hidden-xs mb20">
			<div class="panel-heading">
				<h4 class="panel-title">
					<span class="fa fa-fire"></span> Koleksi Populer
				</h4>	
			</div>
			<div class="panel-body">
				@foreach ($popularMangas as $popularManga)
					<div class="media">
					 	<a class="media-left media-middle" href="/manga/{{$popularManga->slug}}">
					    	<img width="32" height="32" src="{{$popularManga->getCover('small')}}" alt="Cover {{$popularManga->name}}" class="avatar avatar-sm img-circle">
					  	</a>
					  	<div class="media-body">
					    	<h5 class="media-heading"><a href="/manga/{{$popularManga->slug}}"  class="mn-tooltip" data-id="{{$popularManga->id}}">{{$popularManga->name}}</a></h5>
					    	<small>{{str_limit($popularManga->getGenre(),35)}}</small><br>
					    	<small><em>{{humanDate($popularManga->created_at)}}</em></small>
					  	</div>
					</div>
				@endforeach
			</div>
		</div>

		<div class="fb-page mb20" data-href="http://facebook.com/manggaku" data-width="301" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="http://facebook.com/manggaku"><a href="http://facebook.com/manggaku">Manggaku - Read Manga Online</a></blockquote></div></div>

		<!-- <div data-manggaku="load" data-target="widget-login"></div> -->
		
		<div class="mb20">
			<script src="http://scr.kliksaya.com/js-ad.php?zid=157592" type="text/javascript"></script>
		</div>

		<div class="panel panel-primary hidden-xs">
			<div class="panel-heading">
				<h4 class="panel-title">
					<span class="fa fa-bolt"></span> Koleksi Baru
				</h4>	
			</div>
			<div class="panel-body">
				@foreach ($newMangas as $newManga)
					<div class="media">
					 	<a class="media-left media-middle" href="/manga/{{$newManga->slug}}">
					    	<img width="32" height="32" src="{{$newManga->getCover('small')}}" alt="Cover {{$newManga->name}}" class="avatar avatar-sm img-circle">
					  	</a>
					  	<div class="media-body">
					    	<h5 class="media-heading"><a href="/manga/{{$newManga->slug}}"  class="mn-tooltip" data-id="{{$newManga->id}}">{{$newManga->name}}</a></h5>
					    	<small>{{str_limit($newManga->getGenre(),35)}}</small><br>
					    	<small><em>{{humanDate($newManga->created_at)}}</em></small>
					  	</div>
					</div>
				@endforeach
			</div>
		</div>
	</div>
</div>
@include('_footer')