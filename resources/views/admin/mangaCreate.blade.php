@include('admin._header')
	<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<section class="panel">
			<header class="panel-heading">Create Manga</header>
			{!!Form::open(array('route'=>'prabu.manga.store','files' => true,'class'=>'form-horizontal'))!!}
			<div class="panel-body">
				@if (count($errors) > 0)
				    <div class="alert alert-danger">
				            @foreach ($errors->all() as $error)
				                {{ $error }}<br>
				            @endforeach
				    </div>
				@endif
				<div class="form-group">
					<label class="col-sm-2 control-label">Name</label>
					<div class="col-sm-10">
						{!!Form::input('text', 'name', null, ['class' => 'form-control'])!!}
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Alt Name</label>
					<div class="col-sm-10">
						{!! Form::textarea('alt_name', null, ['class' => 'form-control','size' => '30x3']) !!}
						<p class="help-block">Satu Nama Setiap Baris</p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Author</label>
					<div class="col-sm-10">
						<select class="form-control chosen" name="author">
							@foreach($creators->get() as $creator)
								<option value="{{$creator->id}}">{{$creator->name}}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Artist</label>
					<div class="col-sm-10">
						<select class="form-control chosen" name="artist">
							@foreach($creators->get() as $creator)
								<option value="{{$creator->id}}">{{$creator->name}}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Released</label>
					<div class="col-sm-10">
						{!!Form::input('text', 'released', null, ['class' => 'form-control'])!!}
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Plot</label>
					<div class="col-sm-10">
						{!! Form::textarea('plot', null, ['class' => 'form-control','size' => '30x3']) !!}
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Type</label>
					<div class="col-sm-10">
						<select class="form-control chosen" name="type">
							@foreach($types->get() as $type)
								<option value="{{$type->id}}">{{$type->name}}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Genre</label>
					<div class="col-sm-10">
						<select class="form-control chosen" multiple name="genre[]" data-placeholder="ketikkan Genre">
							@foreach($genres->get() as $genre)
								<option value="{{$genre->id}}">{{$genre->name}}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Status</label>
					<div class="col-sm-10">
						<select class="form-control chosen" name="status">
							@foreach($statuses->get() as $status)
								<option value="{{$status->id}}">{{$status->name}}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Cover URL</label>
					<div class="col-sm-10">
						{!!Form::input('text', 'coverURL', null, ['class' => 'form-control'])!!}
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Cover</label>
					<div class="col-sm-10">
						<input type="file" name="cover">
					</div>
				</div>
			</div>
			<div class="panel-footer text-right">
				<input type="submit" class="btn btn-primary" value="Simpan" />
			</div>
			</form>
		</section>
	</div>
</div>
@include('admin._footer')