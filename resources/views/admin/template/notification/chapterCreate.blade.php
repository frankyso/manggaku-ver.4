<li class="list-group-item">
	<a href="/prabu/redirect/{{$notification->id}}">
		<span class="pull-left mt5 mr15">
			<img src="{{$notification->manga->getCover('small')}}" class="avatar avatar-sm img-circle" alt="">
		</span>
		<div class="m-body">
			<div class="">
				<small><b>Chapter Baru!</b></small>
			</div>
			<span>{{$notification->manga->name}} Chapter {{$notification->chapter->chapter}}</span>
			<span class="time small">{{humanDate($notification->created_at)}}</span>
		</div>
	</a>
</li>