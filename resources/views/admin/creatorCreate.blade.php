@include('admin._header')
	<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<section class="panel">
			<header class="panel-heading">Create Creator</header>
			{!!Form::open(array('route'=>'prabu.creator.store','files' => true,'class'=>'form-horizontal'))!!}
			<div class="panel-body">
				@if (count($errors) > 0)
				    <div class="alert alert-danger">
				            @foreach ($errors->all() as $error)
				                {{ $error }}<br>
				            @endforeach
				    </div>
				@endif
				<div class="form-group">
					<label class="col-sm-2 control-label">Name</label>
					<div class="col-sm-10">
						{!!Form::input('text', 'name', null, ['class' => 'form-control'])!!}
					</div>
				</div>
			</div>
			<div class="panel-footer text-right">
				<input type="submit" class="btn btn-primary" value="Simpan" />
			</div>
			</form>
		</section>
	</div>
</div>
@include('admin._footer')