@include('admin._header')
	<div class="row">
		<div class="col-md-4">
			{!!Form::open(array('route'=>['prabu.chapter.update',$chapter->id],'method'=>'PUT','files' => true))!!}
			<div class="panel panel-default">
				<div class="panel-heading">
					Chapter Detail
				</div>
				<div class="panel-body">
					<div class="form-group">
						<label class="control-label">Chapter</label>
							{!!Form::input('text', 'chapter', $chapter->chapter, ['class' => 'form-control'])!!}
						
					</div>
					<div class="form-group">
						<label class="control-label">Name</label>
							{!!Form::input('text', 'name', $chapter->name, ['class' => 'form-control'])!!}
						
					</div>
					<hr>
					<div class="form-group">
						<label class="control-label">Manga File</label>
							<input type="file" name="file[]" multiple>
						
					</div>
					<div class="form-group">
						<label class="control-label">URL Scrap</label>
							{!!Form::input('text', 'url', null, ['class' => 'form-control'])!!}
							<small>Scrap From Single Page <span class="text-danger">Please Leave Manga File Empty</span></small>
						
					</div>
					<div class="form-group">
						<label class="control-label">Manga Directory</label>
							{!!Form::input('text', 'directory', null, ['class' => 'form-control','disabled'])!!}
							<input type="checkbox" name="fromDirectory" value="1"> Scrap From Manga Directory
						
					</div>
					<hr>
					<div class="form-group">
 						<input type="checkbox" name="deleteOldFile" value="1"> Hapus File Lama
 					</div>
				</div>
				<div class="panel-footer">
					<input type="submit" name="submit" value="Simpan Perubahan" class="btn btn-success btn-block">
				</div>
			</div>
			</form>
		
			<form action="/prabu/publish/setimage" method="POST">
			<div class="panel panel-default">
				<div class="panel-heading">
					Publish To Social Media
				</div>
				<div class="panel-body">
					<div class="form-group">
						<input type="hidden" name="id_manga" value="{{$chapter->manga->id}}">
						<input type="hidden" name="id_chapter" value="{{$chapter->id}}">
						<label>Pilih Image Index yang ingin di publish</label>
						<select name="imageToPublish" id="" class="form-control">
							@foreach($mangaLists as $key => $mangaList)
								<option value="{{config('app.manga_remote_url')."/".$mangaList}}">Index {{$key}}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="panel-footer">
					<input type="submit" name="submit" value="Publish" class="btn btn-success btn-block">
				</div>
			</div>
			</form>
			<div class="panel panel-default">
				<div class="panel-body">
					{!!Form::open(array('route'=>['prabu.chapter.destroy',$chapter->id],'method'=>'DELETE'))!!}
						<input type="submit" name="submit" value="Hapus Chapter" class="btn btn-danger btn-block">
					</form>
				</div>	
			</div>
		</div>
		<div class="col-md-8">
			<div class="row">
				@foreach($mangaLists as $key => $mangaList)
					<div class="col-md-4 mb30">
						<img src="{{config('app.manga_remote_url')."/".$mangaList}}" alt="" width="100%" height="300px;" style="height:300px !important;">
						<div class="alert alert-danger">
							Index - {{$key}}
						</div>
					</div>
				@endforeach
			</div>
		</div>
	</div>
@include('admin._footer')