@include('admin._header')
	<section class="panel">
		<header class="panel-heading">Mangas Lists</header>
		<div class="panel-body">
			<div class="row mb20">
				<div class="col-md-4">
					<a href="/prabu/manga/create" class="btn btn-primary"><span class="ti-plus"></span> Create Manga</a>
				</div>
				<div class="col-md-8 text-right">
					<form class="form-inline" role="form" method="get">
						<div class="form-group mr5">
							<input type="text" name="q" class="form-control" placeholder="Search" value="{{$q}}">
						</div>

						<button type="submit" class="btn btn-default">Search</button>
					</form>
				</div>
			</div>
			<table class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>Title</th>
						<th>Author</th>
						<th>Artist</th>
						<th>Released</th>
						<th>Rating</th>
						<th>Created At</th>
						<th width="5%"></th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th>Title</th>
						<th>Author</th>
						<th>Artist</th>
						<th>Released</th>
						<th>Rating</th>
						<th>Created At</th>
						<th width="5%"></th>
					</tr>
				</tfoot>
				<tbody>
					@foreach($mangas as $manga)
					<tr>
						<td>{{$manga->name}}</td>
						<td>{{$manga->getAuthor['name']}}</td>
						<td>{{$manga->getArtist['name']}}</td>
						<td>{{$manga->released}}</td>
						<td>{{$manga->rate}}</td>
						<td>{{$manga->created_at}}</td>
						<td><a href="{{route('prabu.manga.show',$manga->id)}}" class="btn btn-sm btn-default">Lihat Manga</a></td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
		<div class="panel-footer">
			{!! $mangas->render() !!}
		</div>
	</section>
@include('admin._footer')