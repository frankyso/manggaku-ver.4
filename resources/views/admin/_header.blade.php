<!doctype html>
<html class="no-js" lang="">
	<head>
 
	<meta charset="utf-8">
	<meta name="description" content="Flat, Clean, Responsive, application admin template built with bootstrap 3">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
 
	<title>Manggaku - Admin Dashboard</title> 
	<link rel="stylesheet" href="/css/admin.css">
 
	<!--[if lt IE 9]>
	        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	    <![endif]-->
</head>
 
<body>
	<div class="app">
	<header class="header header-fixed navbar">
	<div class="brand"> 
		<a href="javascript:;" class="ti-menu off-left visible-xs" data-toggle="offscreen" data-move="ltr"></a>
		<a href="/prabu" class="navbar-brand">
			<img src="/img/logo.png" alt="">
			<span class="heading-font">
				Manggaku
			</span>
		</a>

	</div>
	<ul class="nav navbar-nav">
		<li class="hidden-xs">

			<a href="javascript:;" class="toggle-sidebar">
				<i class="ti-menu"></i>
			</a>

		</li>
		<li class="header-search">

			<a href="javascript:;" class="toggle-search">
				<i class="ti-search"></i>
			</a>

			<div class="search-container">
				<form role="search">
					<input type="text" class="form-control search" placeholder="type and press enter">
				</form>
			</div>
		</li>
	</ul>
	<ul class="nav navbar-nav navbar-right">
		<li class="dropdown hidden-xs">
			<a href="javascript:;" data-toggle="dropdown">
				<i class="ti-more-alt"></i>
			</a>
			<ul class="dropdown-menu animated zoomIn">
				<li class="dropdown-header">Quick Links</li>
				<li>
					<a href="javascript:;">Start New Campaign</a>
				</li>
				<li>
					<a href="javascript:;">Review Campaigns</a>
				</li>
				<li class="divider"></li>
				<li>
					<a href="javascript:;">Settings</a>
				</li>
				<li>
					<a href="javascript:;">Wish List</a>
				</li>
				<li>
					<a href="javascript:;">Purchases History</a>
				</li>
				<li class="divider"></li>
				<li>
					<a href="javascript:;">Activity Log</a>
				</li>
				<li>
					<a href="javascript:;">Settings</a>
				</li>
				<li>
					<a href="javascript:;">System Reports</a>
				</li>
				<li class="divider"></li>
				<li>
					<a href="javascript:;">Help</a>
				</li>
				<li>
					<a href="javascript:;">Report a Problem</a>
				</li>
			</ul>
		</li>
		<li class="notifications dropdown">
			<a href="javascript:;" data-toggle="dropdown">
				<i class="ti-bell"></i>
				@if(Auth::user()->notification()->unreaded()->count()>0)
				<div class="badge badge-top bg-danger animated flash">
					<span>{{Auth::user()->notification()->unreaded()->count()}}</span>
				</div>
				@endif
			</a>
			<div class="dropdown-menu animated fadeInLeft">
				<div class="panel panel-default no-m">
					<div class="panel-heading small"><b>Notifications</b>
					</div>
					<ul class="list-group">
						@foreach(Auth::user()->notification()->unreaded()->take(10)->get() as $notification)
							{!!$notification->parseForAdmin()!!}
						@endforeach
						<!-- <li class="list-group-item">
							<a href="javascript:;">
								<div class="pull-left mt5 mr15">
									<div class="circle-icon bg-danger">
										<i class="ti-download"></i>
									</div>
								</div>
								<div class="m-body">
									<span>Upload Progress</span>
									<div class="progress progress-xs mt5 mb5">
										<div class="progress-bar" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
										</div>
									</div>
									<span class="time small">Submited 23 mins ago</span>
								</div>
							</a>
						</li>
						<li class="list-group-item">
							<a href="javascript:;">
								<span class="pull-left mt5 mr15">
									<img src="img/face3.jpg" class="avatar avatar-sm img-circle" alt="">
								</span>
								<div class="m-body">
									<em>Status Update:</em>
									<span>All servers now online</span>
									<span class="time small">5 days ago</span>
								</div>
							</a>
						</li> -->
					</ul>
					<div class="panel-footer">
						<a href="javascript:;">See all notifications</a>
					</div>
				</div>
			</div>
		</li>
		<li class="off-right">
			<a href="javascript:;" data-toggle="dropdown">
				<img src="/img/avatar.jpg" class="header-avatar img-circle" alt="user" title="user">
				<span class="hidden-xs ml10">Franky Soputra</span>
				<i class="ti-angle-down ti-caret hidden-xs"></i>
			</a>
			<ul class="dropdown-menu animated fadeInRight">
				<li>
					<a href="javascript:;">Settings</a>
				</li>
				<li>
					<a href="javascript:;">Upgrade</a>
				</li>
				<li>
					<a href="javascript:;">
						<div class="badge bg-danger pull-right">3</div>
						<span>Notifications</span>
					</a>
				</li>
				<li>
					<a href="javascript:;">Help</a>
				</li>
				<li>
					<a href="/logout">Logout</a>
				</li>
			</ul>
		</li>
	</ul>
</header>

<section class="layout">

	<aside class="sidebar offscreen-left">

		<nav class="main-navigation" data-height="auto" data-size="6px" data-distance="0" data-rail-visible="true" data-wheel-step="10">
			<p class="nav-title">MENU</p>
			<ul class="nav">
				<li>
					<a href="{{route('prabu.overview.index')}}">
						<i class="ti-home"></i>
						<span>Dashboard</span>
					</a>
				</li>
				
				<li>
					<a href="{{route('prabu.user.index')}}">
						<i class="ti-user"></i>
						<span>User</span>
					</a>
				</li>

				<!-- <li>
					<a href="javascript:;">
						<i class="toggle-accordion"></i>
						<i class="ti-window"></i>
						<span>Interface</span>
					</a>
					<ul class="sub-menu">
						<li>
							<a href="">
								<span>Slider</span>
							</a>
						</li>
					</ul>
				</li> -->

				<li>
					<a href="javascript:;">
						<i class="toggle-accordion"></i>
						<i class="ti-folder"></i>
						<span>Manga</span>
					</a>
					<ul class="sub-menu">
						<li>
							<a href="{{route('prabu.manga.index')}}">
								<span>Manga</span>
							</a>
						</li>
						<li>
							<a href="{{route('prabu.creator.index')}}">
								<span>Creator</span>
							</a>
						</li>
						<li>
							<a href="">
								<span>Genre</span>
							</a>
						</li>
						<li>
							<a href="">
								<span>Tipe</span>
							</a>
						</li>
						<li>
							<a href="">
								<span>Status</span>
							</a>
						</li>
					</ul>
				</li>

				<li>
					<a href="javascript:;">
						<i class="toggle-accordion"></i>
						<i class="ti-basketball"></i>
						<span>Lainnya</span>
					</a>
					<ul class="sub-menu">
						<li>
							<a href="">
								<span>Cron Job</span>
							</a>
						</li>
					</ul>
				</li>
			</ul>
		</nav>
	</aside>


	<section class="main-content">

		<div class="content-wrap">

			<div class="wrapper">