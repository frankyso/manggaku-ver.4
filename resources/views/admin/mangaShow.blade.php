@include('admin._header')
	<div class="row">
		<div class="col-md-4">
			<div class="panel panel-default">
				<div class="panel-body">
					<img src="{{$manga->getCover()}}" onError="this.src='/img/cover/original/not-available.jpg'" alt="Cover Manga {{$manga->name}}" class="img img-responsive img-thumbnail">
				</div>
				<div class="list-group">
					<div href="javascript:;" class="list-group-item">
						<i class="ti-ink-pen mr10 text-muted"></i>Author : {{$manga->getAuthor->name}}
					</div>
					<div href="javascript:;" class="list-group-item">
						<i class="ti-brush mr10 text-muted"></i>Artist : {{$manga->getArtist->name}}
					</div>
					<div href="javascript:;" class="list-group-item">
						<i class="ti-calendar mr10 text-muted"></i>Released : {{$manga->released}}
					</div>
					<div href="javascript:;" class="list-group-item">
						<i class="ti-tag mr10 text-muted"></i>Genre : 
						@foreach($manga->genre()->get() as $genreRelation)
							<a href="/directory/genre/{{strtolower($genreRelation->genre['name'])}}" class="label label-default">{{$genreRelation->genre['name']}}</a>
						@endforeach
					</div>
					<div href="javascript:;" class="list-group-item">
						<i class="ti-flag mr10 text-muted"></i>Status : <span class="label label-{{$manga->status->color}}">{{$manga->status->name}}</span>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-8">
			<div class="box-tab">
				<ul class="nav nav-tabs">
					<li class="active">
						<a href="#home" data-toggle="tab" aria-expanded="true">Manga</a> 
					</li> 
					<li class="">
						<a href="#chapter" data-toggle="tab" aria-expanded="false">Chapter</a> 
					</li>
					<li class="">
						<a href="#schedule" data-toggle="tab" aria-expanded="false">Schedule</a> 
					</li> 
				</ul>
				<div class="tab-content">
					<div class="tab-pane fade active in" id="home">
						{!!Form::open(array('route'=>['prabu.manga.update',$manga->id],'method'=>'PUT','files' => true,'class'=>'form-horizontal'))!!}
							@if (count($errors) > 0)
							    <div class="alert alert-danger">
							            @foreach ($errors->all() as $error)
							                {{ $error }}<br>
							            @endforeach
							    </div>
							@endif
							<div class="form-group">
								<label class="col-sm-2 control-label">Name</label>
								<div class="col-sm-10">
									{!!Form::input('text', 'name', $manga->name, ['class' => 'form-control'])!!}
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Alt Name</label>
								<div class="col-sm-10">
									{!! Form::textarea('alt_name', $manga->alt_name, ['class' => 'form-control','size' => '30x3']) !!}
									<p class="help-block">Satu Nama Setiap Baris</p>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Author</label>
								<div class="col-sm-10">
									<select class="form-control chosen" name="author">
										@foreach($creators->get() as $creator)
											<option @if($creator->id == $manga->author) selected @endif value="{{$creator->id}}">{{$creator->name}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Artist</label>
								<div class="col-sm-10">
									<select class="form-control chosen" name="artist">
										@foreach($creators->get() as $creator)
											<option @if($creator->id == $manga->artist) selected @endif value="{{$creator->id}}">{{$creator->name}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Released</label>
								<div class="col-sm-10">
									{!!Form::input('text', 'released', $manga->released, ['class' => 'form-control'])!!}
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Plot</label>
								<div class="col-sm-10">
									{!! Form::textarea('plot', $manga->plot, ['class' => 'form-control','size' => '30x3']) !!}
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Type</label>
								<div class="col-sm-10">
									<select class="form-control chosen" name="type">
										@foreach($types->get() as $type)
											<option @if($type->id == $manga->id_type) selected @endif value="{{$type->id}}">{{$type->name}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Genre</label>
								<div class="col-sm-10">
									<select class="form-control chosen" multiple name="genre[]" data-placeholder="ketikkan Genre">
										@foreach($genres->get() as $genre)?>
											<option @if($manga->genre()->where('id_genre','=',$genre->id)->count()) selected @endif value="{{$genre->id}}">{{$genre->name}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Status</label>
								<div class="col-sm-10">
									<select class="form-control chosen" name="status">
										@foreach($statuses->get() as $status)
											<option @if($status->id == $manga->id_status) selected @endif value="{{$status->id}}">{{$status->name}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Cover URL</label>
								<div class="col-sm-10">
									{!!Form::input('text', 'coverURL', null, ['class' => 'form-control'])!!}
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Cover</label>
								<div class="col-sm-10">
									<input type="file" name="cover">
								</div>
							</div>
							<hr>

							<div class="form-group">
								<div class="col-md-2">
									
								</div>
								<div class="col-md-10">
									<label for="">
										<input type="checkbox" value="1" name="scrap_active" @if($manga->scrapper->active) checked @endif> Activate Scrappeer
									</label>
								</div>
							</div>
							@if($manga->scrapper->active)
							<div class="form-group">
								<label class="col-sm-2 control-label">Scrap Directory</label>
								<div class="col-sm-10">
									{!!Form::input('text', 'scrap', $manga->scrapper->source, ['class' => 'form-control','placeholder'=>'ex. www.pecintakomik.com/manga/one_piece'])!!}
									<small>Currently Support : www.pecintakomik.com, www.komikid.com, awmanga.me, www.awmanga.me</small>
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-2 control-label">Scrap Interval</label>
								<div class="col-sm-10">
									{!!Form::input('text', 'scrap.interval', ($manga->scrapper->interval/86400), ['class' => 'form-control'])!!}
								</div>
							</div>
							@endif
							<div class="form-group">
								<div class="col-md-12 text-right">
									<input type="submit" value="Simpan Pembaruan" class="btn btn-success">
								</div>
							</div>
						</form>
					</div> 
					<div class="tab-pane fade" id="chapter">
						<a href="" class="btn btn-primary" data-toggle="modal" data-target="#uploadChapter">Upload</a> 
						<div class="clearfix mt15"></div>
						<table class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>Chapter</th>
									<th>Name</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								@foreach($manga->chapter()->orderBy('chapter','desc')->get() as $chapter)
								<tr>
									<td>{{$chapter->chapter}}</td>
									<td>{{$chapter->name}}</td>
									<td><a href="{{route('prabu.chapter.show',$chapter->id)}}" class="btn btn-sm btn-default">Lihat</a></td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div> 
					<div class="tab-pane fade" id="schedule">
						{!!Form::open(array('url'=>'prabu/chapterbatch','method'=>'POST','files' => true,'class'=>'form-horizontal'))!!}
							<input type="hidden" value="{{$manga->id}}" name="id_manga">
							<div class="form-group">
								<label class="col-sm-2 control-label">Chapter</label>
								<div class="col-sm-10">
									{!!Form::input('text', 'chapter', null, ['class' => 'form-control'])!!}
								</div>
							</div>

							<div class="form-group tochapter collapse">
								<label class="col-sm-2 control-label">Ke Chapter</label>
								<div class="col-sm-10">
									{!!Form::input('text', 'tochapter', null, ['class' => 'form-control'])!!}
								</div>
							</div>

							<div class="form-group tochapter collapse">
								<label class="col-sm-2 control-label">Jarak Hari</label>
								<div class="col-sm-10">
									{!!Form::input('text', 'interval', null, ['class' => 'form-control'])!!}
								</div>
							</div>

							<div class="form-group collapse in tanggal">
								<label class="col-sm-2 control-label">Tanggal</label>
								<div class="col-sm-10">
									{!!Form::input('text', 'date', date("m/d/Y"), ['class' => 'form-control datepicker'])!!}
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label"></label>
								<div class="col-sm-10">
									<div class="checkbox" data-toggle="collapse" data-target=".tochapter, .tanggal">
									    <label>
									    	<input type="checkbox" name="batch" value="1"> Batch Manga (Tidak dapat di gunakan pada Scrap URL)
									   	</label>
									</div>
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-2 control-label">Name</label>
								<div class="col-sm-10">
									{!!Form::input('text', 'name', null, ['class' => 'form-control'])!!}
								</div>
							</div>
							<hr>
							<div class="form-group">
								<label class="col-sm-2 control-label">URL Scrap</label>
								<div class="col-sm-10">
									{!!Form::input('text', 'url', null, ['class' => 'form-control'])!!}
									<small>Scrap From Single Page <span class="text-danger">Please Leave Manga File Empty</span></small>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Manga Directory</label>
								<div class="col-sm-10">
									{!!Form::input('text', 'directory', null, ['class' => 'form-control','disabled'])!!}
									<input type="checkbox" name="fromDirectory" value="1"> Scrap From Manga Directory
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label"></label>
								<div class="col-sm-10">
									<input type="submit" value="Simpan" class="btn btn-primary">
								</div>
							</div>
						</form>
						<div class="clearfix mt15"></div>
						<table class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>Chapter</th>
									<th>URL</th>
									<th>Execute</th>
								</tr>
							</thead>
							<tbody>
								@foreach($manga->grablist()->orderBy('execute_at','asc')->get() as $batch)
								<tr>
									<td>{{$batch->chapter}}</td>
									<td>{{$batch->source_url}}</td>
									<td>{{humanDate($batch->execute_at)}}</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div> 
				</div>
		</div>
	</div>
	{!!Form::open(array('route'=>['prabu.chapter.store'],'method'=>'POST','files' => true,'class'=>'form-horizontal'))!!}
	<div class="modal fade" id="uploadChapter">
		<div class="modal-dialog">
	    	<div class="modal-content">
	      		<div class="modal-header">
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        		<h4 class="modal-title">Upload Chapter</h4>
	      		</div>
	      		<div class="modal-body">
					<input type="hidden" value="{{$manga->id}}" name="id_manga">
					<div class="form-group">
						<label class="col-sm-2 control-label">Chapter</label>
						<div class="col-sm-10">
							{!!Form::input('text', 'chapter', null, ['class' => 'form-control'])!!}
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Name</label>
						<div class="col-sm-10">
							{!!Form::input('text', 'name', null, ['class' => 'form-control'])!!}
						</div>
					</div>
					<hr>
					<div class="form-group">
						<label class="col-sm-2 control-label">Manga File</label>
						<div class="col-sm-10">
							<input type="file" name="file[]" multiple>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">URL Scrap</label>
						<div class="col-sm-10">
							{!!Form::input('text', 'url', null, ['class' => 'form-control'])!!}
							<small>Scrap From Single Page <span class="text-danger">Please Leave Manga File Empty</span></small>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Manga Directory</label>
						<div class="col-sm-10">
							{!!Form::input('text', 'directory', null, ['class' => 'form-control','disabled'])!!}
							<input type="checkbox" name="fromDirectory" value="1"> Scrap From Manga Directory
						</div>
					</div>
	      		</div>
	      		<div class="modal-footer">
	      			<input type="submit" name="submit" value="Upload Chapter" class="btn btn-success">
	      		</div>
	    	</div>
	  	</div>
	</div>
	</form>

	
@include('admin._footer')