@include('admin._header')
	<div class="row">
		<div class="col-md-6">
			{!!Form::open(array('route'=>['prabu.overview.update',$manga->id],'method'=>'PUT','files' => true))!!}
			<div class="panel panel-default">
				<div class="panel-heading">
					Reset Manga Cover
				</div>
				<div class="panel-body">
					<div class="form-group">
						<div class="col-md-12">
							<h4>Manga : {{$manga->name}}</h4>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Cover URL</label>
						<div class="col-sm-10">
							{!!Form::input('text', 'coverURL', null, ['class' => 'form-control'])!!}
						</div>
					</div>
				</div>
				<div class="panel-footer">
					<input type="submit" value="Submit" class="btn btn-primary">
				</div>
			</div>
		</form>
		</div>
	</div>
@include('admin._footer')