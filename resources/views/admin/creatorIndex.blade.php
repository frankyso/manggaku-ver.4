@include('admin._header')
	<section class="panel">
		<header class="panel-heading">Creators Lists</header>
		<div class="panel-body">
			<div class="row mb20">
				<div class="col-md-4">
					<a href="/prabu/creator/create" class="btn btn-primary"><span class="ti-plus"></span> Create Creator</a>
				</div>
				<div class="col-md-8 text-right">
					<form class="form-inline" role="form">
						<div class="form-group mr5">
							<input type="email" class="form-control" id="exampleInputEmail2" placeholder="Search">
						</div>

						<button type="submit" class="btn btn-default">Search</button>
					</form>
				</div>
			</div>
			<table class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>Name</th>
						<th width="5%"></th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th>Name</th>
						<th width="5%"></th>
					</tr>
				</tfoot>
				<tbody>
					@foreach($creators as $creator)
					<tr>
						<td>{{$creator->name}}</td>
						<td><a href="{{route('prabu.creator.show',$creator->id)}}" class="btn btn-sm btn-default">Lihat creator</a></td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
		<div class="panel-footer">
			{!! $creators->render() !!}
		</div>
	</section>
@include('admin._footer')