<!doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
    <meta http-equiv="Pragma" content="no-cache"/>
    <meta http-equiv="Cache-Control" content="no-cache"/>

    <title>{{$page_title}}</title>
    <meta http-equiv="keywords" name="keywords" content="{{$page_keywords}}"/>
    <meta http-equiv="description" name="description" content="{{$page_description}}"/>
    
    <meta content='INDEX,FOLLOW' name='Robots'/>
    <meta content='INDEX, FOLLOW' name='GOOGLEBOT'/>
    <meta content='INDEX, FOLLOW' name='msnbot'/>
    <meta content='INDEX, FOLLOW' name='Googlebot-Image'/>
    <meta content='INDEX, FOLLOW' name='Slurp'/>
    <meta content='INDEX, FOLLOW' name='ZyBorg'/>
    <meta content='INDEX, FOLLOW' name='Scooter'/>
    <meta content='ALL' name='spiders'/>
    <meta content='ALL' name='webcrawlers'/>
    <meta content='1 day' name='Revisit-after'/>
    <meta content='never' name='expires'/>
    <meta content='0' http-equiv='expires'/>
    <meta content='Global' name='Distribution'/>
    <meta content='true' name='MSSmartTagsPreventParsing'/>
    <meta content='general' name='rating'/>
    <meta content='Aeiwi, Alexa, Alltheweb, altavista, AOL netfind, anzwers, Canada, directhit, euroseek, excite, overture, go, google, hotbot, infomax, kanoodle, lycos, mastersite, national directory, northern light, searchit, simplesearch, websmostlinked, webtop, what-u-seek, AOL, Yahoo, Webcrawler, infoseek, excite, magellan, looksmart, CNET' name='search engines'/>

    <meta property="og:type" content="website" />
    <meta property="og:og:site_name" content="Manggaku" />
    <meta property="og:title" content="{{$page_og_title}}" />
    <meta property="og:image" content="{{$page_og_image}}" />
    <meta property="fb:app_id" content="495377260495706"/>
    <link rel="shortcut icon" href="/img/logo.png" alt="Logo Manggaku"/>
    <link rel="stylesheet" href="/css/main.css">
    <script src="https://apis.google.com/js/platform.js" async defer></script>

    <script type="text/javascript">var switchTo5x=true;</script>
    <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
    <script type="text/javascript">stLight.options({publisher: "bc4c0117-5136-4236-9d75-9bf3cb6106c7", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
</head>

<body>
    <div id="fb-root"></div>
    <script>
   window.fbAsyncInit = function() {
        FB.init({
            appId: '495377260495706',
            cookie: true, xfbml: true, oauth: true 
        });

        FB.Event.subscribe('auth.login', function(response) {
            window.location.href='http://www.manggaku.com/auth/facebook';
        });

        FB.Event.subscribe('auth.logout', function(response) {
            window.location.href='http://www.manggaku.com/logout';
        });

        FB.Event.subscribe('comment.create', function(response){
            $.post("/ajax/comment", { response:response, type:"create"} ,function(data){});
        });
        FB.Event.subscribe('comment.remove', function(response){
            $.post("/ajax/comment", { response:response, type:"remove"} ,function(data){});
        });
    };



    (function() {
        var e = document.createElement('script'); e.async = true;
        e.src = document.location.protocol +
        '//connect.facebook.net/en_US/all.js#xfbml=1&appId=495377260495706';
        document.getElementById('fb-root').appendChild(e);
    }());

    function fblogin() {
        FB.login(function(response) {
            window.top.location = "http://www.manggaku.com/auth/facebook";
        });
    }    
</script>

    <div class="modal fade" tabindex="-1" role="dialog" id="loginModal">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="mt0">Login atau Register</h4>
            <p>Gunakan Social media Anda untuk login ke Manggaku</p>
            <hr>
            <a href="{{url('/auth/facebook')}}" class="btn btn-social btn-facebook btn-lg btn-block"><i class="fa fa-facebook"></i> Login Dengan Facebook </a>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div class="modal fade" tabindex="-1" role="dialog" id="pointExplain">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="mt0">Apa itu Point?</h4>
            <p>Point yang anda kumpulkan dapat digunakan untuk mendownload chapter manga yang ada. (Under Develop)</p>
            <h4>Bagaimana cara mengumpulkannya?</h4>
            <p>Cara mengumpulkan poin sangatlah mudah, silahkan melihat table di bawah ini untuk jumlah point yang Anda dapatkan dengan persyaratan.</p>
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <td>Persyaratan</td>
                        <td>Poin</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Membaca Manga</td>
                        <td>+1 perhalaman</td>
                    </tr>
                    <tr>
                        <td>Memberikan Rating pada Manga</td>
                        <td>+10</td>
                    </tr>
                    <tr>
                        <td>Memberikan Komentar</td>
                        <td>+25</td>
                    </tr>
                </tbody>
            </table>            
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div class="app">
        <header class="header navbar">
            <div class="container">
                <div class="brand">
                    <a href="/" class="navbar-brand text-center">
                        <img src="/img/logo.png" alt="">
                        <span class="heading-font">
                            Manggaku
                        </span>
                    </a>
                    <a href="javascript:;" class="ti-menu off-right visible-xs" data-toggle="offscreen" data-move="rtl"></a>
                </div>
                <ul class="nav navbar-nav">
                    <!-- <li class="header-search">
                        <a href="javascript:;" class="toggle-search">
                            <i class="ti-search"></i>
                        </a>

                        <div class="search-container">
                            <form role="search" action="/search">
                                <input type="text" class="form-control search" placeholder="Ketik Judul Manga" name="name" id="search-box">
                            </form>
                        </div>
                    </li> -->
                </ul>
                <div class="collapse navbar-collapse" id="main-navigation">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="/">Home</a></li>
                        <li><a href="/directory">Direktori Manga</a></li>
                        <li><a href="/join">Join Team</a></li>
                        <li><a href="/partners">Nakama</a></li>
                        @if(!Auth::check())
                            <li class="btn-facebook" data-toggle="modal" data-target="#loginModal"><a href="#">Login</a></li>
                            <li class="btn-facebook" data-toggle="modal" data-target="#loginModal"><a href="#">Register</a></li>
                        @else
                            <li class="off-right">
                                <a href="javascript:;" data-toggle="dropdown" aria-expanded="false">
                                    <img src="{{Auth::user()->avatar}}" class="header-avatar img-circle" alt="user" title="user">
                                    <span class="hidden-xs ml10">{{Auth::user()->name}}</span> <i class="ti-angle-down ti-caret hidden-xs"></i> 
                                </a> 
                                <ul class="dropdown-menu animated fadeInRight"> 
                                    <li> <a href="{{url('/profile/me')}}">Profile</a></li> 
                                    <li> <a href="{{url('logout')}}">Logout</a></li> 
                                </ul> 
                            </li>
                            <li class="btn-facebook" data-toggle="modal" data-target="#pointExplain"><a href="#">({{Auth::user()->getMeta('point',0)}}) Poin</a></li>
                        @endif
                    </ul>
                </div>
            </div>
        </header>

        <div class="container layout">
            <aside class="sidebar offscreen-right hidden-md hidden-lg">
                <nav class="main-navigation" data-height="auto" data-size="6px" data-distance="0" data-rail-visible="true" data-wheel-step="10">
                    <p class="nav-title">MENU</p>
                    <ul class="nav">
                        <li>
                            <a href="">
                                <i class="ti-home"></i>
                                <span>Home</span>
                            </a>
                        </li>
                        <li>
                            <a href="/directory">
                                <i class="ti-list"></i>
                                <span>Direktori Manga</span>
                            </a>
                        </li>
                        <li>
                            <a href="/join">
                                <i class="ti-list"></i>
                                <span>Join Team</span>
                            </a>
                        </li>
                        <li>
                            <a href="/join">
                                <i class="ti-list"></i>
                                <span>Join Team</span>
                            </a>
                        </li>
                        <li>
                            <a href="" data-toggle="modal" data-target="#loginModal">
                                <i class="ti-list"></i>
                                <span>Login</span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </aside>
            <div class="manggaku-container">