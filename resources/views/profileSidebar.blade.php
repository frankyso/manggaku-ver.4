<section class="panel panel-primary no-b">
	<header class="panel-heading clearfix brtl brtr">
		<a href="javascript:;" class="pull-left mr15">
			<img src="{{$profile->avatar}}" class="avatar avatar-md img-circle bordered" alt="">
		</a> 
		<div class="overflow-hidden"> 
			<a href="javascript:;" class="h4 show no-m pt10">{{$profile->name}}</a> 
			<small>Pembaca Setia</small>
		</div> 
	</header> 
	<div class="list-group"> 
		<!-- <a href="javascript:;" class="list-group-item"> 
			<i class="ti-user mr10 text-muted"></i>Profile 
		</a>  -->
		<!-- <a href="javascript:;" class="list-group-item"> 
			<span class="badge bg-danger pull-right">89</span> 
			<i class="ti-star mr10 text-muted"></i>Bookmark
		</a>  -->
		<!-- <a href="javascript:;" class="list-group-item"> 
			<span class="badge bg-danger pull-right">89</span> 
			<i class="ti-tag mr10 text-muted"></i>Baca Nanti 
		</a>  -->
		<a href="javascript:;" class="list-group-item"> 
			<i class="ti-timer mr10 text-muted"></i>Riwayat Bacaan
		</a> 
	</div> 
</section>