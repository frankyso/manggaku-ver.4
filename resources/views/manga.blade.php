@include('_header')
<div class="row">
	<div class="col-md-12">
		<!--  ad tags Size: 728x90 ZoneId:1052096-->
		<script type="text/javascript" src="http://js.adstars.co.id/t/052/096/a1052096.js"></script>
	</div>
</div>
<div class="row mb15">
	<div class="col-xs-6">

	</div>
	<div class="col-xs-6 text-right">
		<div class="btn-group mr5 mt10">
			<button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown">Filter
				<span class="ti-angle-down ml10"></span>
			</button>
			<ul class="dropdown-menu text-left filter" data-option-key="filter" role="menu">
				<li><a href="javascript:;" data-filter="*">Semua</a>
				</li>
				<li><a href="javascript:;" data-filter=".VAL, .A, .B, .C">#, A, B, C</a></li>
				<li><a href="javascript:;" data-filter=".D, .E, .F, .G">D, E, F, G</a></li>
				<li><a href="javascript:;" data-filter=".H, .I, .J, .K">H, I, J, K</a></li>
				<li><a href="javascript:;" data-filter=".L, .M, .N, .O">L, M, N, O</a></li>
				<li><a href="javascript:;" data-filter=".P, .Q, .R, .S">P, Q, R, S</a></li>
				<li><a href="javascript:;" data-filter=".T, .U, .V, .W">T, U, V, W</a></li>
				<li><a href="javascript:;" data-filter=".X, .Y, .Z">X, Y, Z</a></li>
			</ul>
		</div>
	</div>
</div>
<div class="switcher view-grid">
	<div class="row feed">
		<?php #foreach ($MangaList as $mangas): ?>
			<div class="col-sm-4 col-lg-4 col-xs-12 switch-item <?php #($mangas->first_character)=='#'?'VAL':$mangas->first_character?>">
				<section class="panel">
					<div class="thumb">
						<?php #GetCoverMangaLists($mangas->first_character);?>
					</div>
					<div class="panel-body">
						<div class="switcher-content">
							<ul class="list-manga-update">
								<?php #foreach ($mangas->result as $key => $manga): ?>
									<li class="<?php #=($manga->id_status==1)?"ongoing":"completed"?>"><a  class="mn-tooltip" data-id="<?php #$manga->id?>" href="<?php #base_url("manga/".$manga->slug)?>"><?php #$manga->name?></a></li>
								<?php #endforeach ?>
							</ul>
						</div>
					</div>
				</section>
			</div>
		<?php #endforeach ?>
	</div>
</div>
@include('_footer')