@include('_header')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Oops!! Halaman tidak ditemukan
                </div>
                <div class="panel-body">
                    <p>Mungkin yang Anda cari tidak ada disini, coba deh bongkar koleksi manga Manggaku :)</p>
                    <a href="/directory" class="btn btn-primary">Bongkar Koleksi Manga</a>
                </div>
            </div>
        </div>
    </div>
@include('_footer')