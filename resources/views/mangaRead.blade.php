@include('_header')
<script>
	function change_manga(manga)
	{
		document.location = "{{url('manga')}}/" + manga;
	}

	function change_chapter(chapter){
		document.location = "{{url('manga/'.$manga->slug)}}/" + chapter;
	}
	function change_page(page)
	{
		document.location = "{{url('manga/'.$manga->slug.'/'.$chapter)}}/" + page;
	}
	function omvKeyPressed(e) {
		var keyCode = 0;
		if (navigator.appName == "Microsoft Internet Explorer")
		{
			if (!e) {
				var e = window.event;
			}            
			if (e.keyCode)
			{              
				keyCode = e.keyCode;  
				if ((keyCode == 37) || (keyCode == 39)) 
				{   
					window.event.keyCode = 0;  
				}    
			}
			else
			{    
				keyCode = e.which;  
			}   
		} 
		else 
		{  
			if (e.which) {  
				keyCode = e.which;  
			}
			else 
			{ 
				keyCode = e.keyCode;   
			}   
		}        
		switch (keyCode) 
		{           
			case 37:         
			window.location = "{{$prevPageURL}}";            
			return false;        
			case 39:   
			window.location = "{{$nextPageURL}}";         
			return false;      
			default:       
			return true;   
		}   
	}   
	document.onkeydown = omvKeyPressed;
</script>

<h4 class="text-muted"><a href="/manga/{{$manga->slug}}">{{$manga->name}}</a> / <a href="/manga/{{$manga->slug}}/{{$chapter}}/{{$page}}">{{$manga->name}} {{$chapter}} Hal.{{$page}}</a></h4>
<form action="" class="form-inline text-center text-muted mb25" id="MangaControl">
	<div class="form-group mr10">
		<label class="control-label mr5">Manga</label>
		<select class="form-control" onchange="change_manga(this.value)">
			@foreach($mangas as $mangaList)
				<option @if($mangaList->id == $manga->id) selected @endif value="{{$mangaList->slug}}">{{$mangaList->name}}</option>
			@endforeach
		</select>
	</div>

	<div class="form-group mr10">
		<label class="control-label mr5">Chapter</label>
		<select class="form-control" onchange="change_chapter(this.value)">
			@foreach($chapters as $chapterList)
				<option @if($chapterList->chapter == $chapter) selected @endif value="{{$chapterList->chapter}}">{{$chapterList->chapter}}</option>
			@endforeach
		</select>
	</div>

	<div class="form-group mr10">
		<label class="control-label mr5">Page</label>
		<a href="{{$prevPageURL}}" class="btn btn-primary hidden-xs"><span class="ti-angle-left"></span> Prev</a>
		<select class="form-control" onchange="change_page(this.value)">
			@for($i=1; $i <= $pages; $i++)
				<option @if($i == $page) selected @endif value="{{$i}}">{{$i}}</option>
			@endfor
		</select>
		<a href="{{$nextPageURL}}" class="btn btn-primary hidden-xs">Next <span class="ti-angle-right"></span></a>
	</div>
	<!-- <div class="form-group mr10">
		@if(Auth::check())
			<a href="" class="btn btn-primary"><span class="fa fa-download"></span> Download</a>
		@else
			<a href="" class="btn btn-primary" data-toggle="modal" data-target="#loginModal"><span class="fa fa-download"></span> Download</a>
		@endif
	</div> -->
</form>

<!--  ad tags Size: 728x90 ZoneId:1052096-->
<div class="mb25 text-center">
	<script src="http://scr.kliksaya.com/js-ad.php?zid=157593" type="text/javascript"></script>
</div>

<div class="row text-center mb25">
	<div class="col-md-12">
		<a href="{{$nextPageURL}}">
			<img src="{{$imageURL}}" class="img animated fadeIn img-responsive center-block" alt="">
		</a>
	</div>
</div>

<div class="row hidden-md hidden-lg mb25">
	<div class="col-md-6 col-sm-6 col-xs-6">
		<a href="{{$prevPageURL}}" class="btn btn-primary btn-block hidden-lg hidden-md"><span class="ti-angle-left"></span> Prev</a>
	</div>
	<div class="col-md-6 col-sm-6 col-xs-6">
		<a href="{{$nextPageURL}}" class="btn btn-primary btn-block hidden-lg hidden-md">Next <span class="ti-angle-right"></span></a>
	</div>
</div>

<div class="mb25 text-center hidden-sm hidden-xs">
	<script type="text/javascript" src="http://js.adstars.co.id/t/052/096/a1052096.js"></script>
</div>

<!-- <div class="mb20 text-center">
	<script src="http://scr.kliksaya.com/js-ad.php?zid=157593" type="text/javascript"></script>
</div> -->

<form action="" class="form-inline text-center text-muted mb25 hidden-xs">
	<div class="form-group mr10">
		<label class="control-label mr5">Manga</label>
		<select class="form-control" onchange="change_manga(this.value)">
			@foreach($mangas as $mangaList)
				<option @if($mangaList->id == $manga->id) selected @endif value="{{$mangaList->slug}}">{{$mangaList->name}}</option>
			@endforeach
		</select>
	</div>

	<div class="form-group mr10">
		<label class="control-label mr5">Chapter</label>
		<select class="form-control" onchange="change_chapter(this.value)">
			@foreach($chapters as $chapterList)
				<option @if($chapterList->chapter == $chapter) selected @endif value="{{$chapterList->chapter}}">{{$chapterList->chapter}}</option>
			@endforeach
		</select>
	</div>

	<div class="form-group mr10">
		<label class="control-label mr5">Page</label>
		<a href="" class="btn btn-primary"><span class="ti-angle-left"></span> Prev</a>
		<select class="form-control" onchange="change_page(this.value)">
			@for($i=1; $i <= $pages; $i++)
				<option @if($i == $page) selected @endif value="{{$i}}">{{$i}}</option>
			@endfor
		</select>
		<a href="" class="btn btn-primary">Next <span class="ti-angle-right"></span></a>
	</div>
</form>

<div class="clearfix"></div>
<h1><a href="{{url('manga/'.$manga->slug)}}"><small class="text-muted">Baca Manga {{$manga->name}}</small></a></h1>
<p>
	<span class='st_facebook_hcount' displayText='Facebook'></span>
	<span class='st_twitter_hcount' displayText='Tweet'></span>
	<span class='st_sharethis_hcount' displayText='ShareThis'></span>
</p>
<div class="clearfix"></div>
<p class="text-muted mb25"><strong>Tag : </strong>{{$manga->name}} Chapter {{$chapter}}, Baca online komik {{$manga->name}} Chapter {{$chapter}}, Baca manga {{$manga->name}} Chapter {{$chapter}} online, Komik {{$manga->name}} chapter {{$chapter}} bahasa indonesia,	wallpaper {{$manga->name}}, baca manga {{$manga->name}} gratis, sinopsis {{$manga->name}}, alur cerita {{$manga->name}},	{{$manga->name}} online, {{$manga->name}} Chapter {{$chapter}} high quality, Download {{$manga->name}} Chapter {{$chapter}}, Free Download {{$manga->name}} Chapter {{$chapter}}</p>

<div class="panel">
	<div class="panel-body">
		<div class="fb-comments" notify="true" data-href="{{Request::url()}}" data-numposts="10" data-width="100%" data-order-by="reverse_time"></div>
	</div>
</div>
@include('_footer')