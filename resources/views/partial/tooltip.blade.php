<div class="row">
	<div class="col-md-4"><img src="{{$manga->getCover('medium')}}" onError="this.src='/img/cover/original/not-available.jpg" class="img img-responsive" alt=""></div>
	<div class="col-md-8">
		<h4 class="mt0">{{$manga->name}}</h4>
		{{str_limit($manga->plot,150)}}
	</div>
</div>