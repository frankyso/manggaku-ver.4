@include('_header')
<div class="row">
	<div class="col-md-12">
		<!--  ad tags Size: 728x90 ZoneId:1052096-->
		<script type="text/javascript" src="http://js.adstars.co.id/t/052/096/a1052096.js"></script>
	</div>
</div>
<div class="row">
	<div class="col-md-8">
		<div class="panel panel-primary">
			<div class="panel-heading">
			<h1 class="panel-title text-capitalize">Baca {{$manga->type->name}} {{$manga->name}}</h1>
			</div>

			<div class="panel-body">
				@if(!empty($manga->plot))
					<p>{!!nl2br($manga->plot)!!}</p>
				@else
					<p class="text-center mt20 mb25 text-muted">Ringkasan Manga {{$manga->name}} belum tersedia</p>
					<!-- <p class="text-center mt20 mb25"><button data-toggle="modal" data-target="#suggest-description" class="btn btn-warning btn-sm"><span class="ti ti-upload"></span> Kirimkan Ringkasan Cerita</a></button> -->
				@endif
				<p class="small">
					<!-- <a href="#Commentbox" class="text-muted mr10"><i class="ti-comment mr5"></i>Comment</a> -->
					<!-- <a href="javascript:;" class="text-muted mr10"><i class="ti-more mr5"></i>More</a> -->
				</p>
				<p class="mt25">
					<div class="fb-like pull-left mr5" data-href="https://facebook.com/manggaku" data-layout="button_count" data-action="like" data-show-faces="true" data-share="false"></div>
					<div class="pull-left mr5">
						<g:follow href="https://plus.google.com/109055500134648115988" rel="publisher"></g:follow> 
					</div>
					<div class="pull-left mr5">
						<a class="twitter-follow-button"
						  href="https://twitter.com/TweetManggaku"
						  data-show-count="true"
						  data-lang="en">
						Follow @TweetManggaku
						</a>
					</div>
				</p>
				<div class="clearfix"></div>
				<hr>
				@if($adult)
				<div class="alert alert-danger" id="alertContent">
					<p>Perhatian pembaca dibawah umur : Manga {{$manga->name}} mengandung konten yang tidak sesuai dengan pembaca dibawah umur, kami menutup ini untuk perlindungan mereka. Jika anda 18 tahun atau lebih, <a href="#alertContent, #chapters" data-toggle="collapse"><strong>Klik disini untuk membaca.</strong></a></p>
				</div>
				@endif
				@if ($manga->chapter->count()>0)
				<ul class="list-manga-update @if($adult) collapse @endif" id="chapters">
					<li><a href="javascript:return false;">Daftar Chapter {{$manga->name}}</a>
						<ul>
							@foreach($manga->chapter()->orderBy('chapter','DESC')->get() as $chapter)
								<li><a href="/manga/{{$manga->slug}}/{{$chapter->chapter}}">{{$manga->name}} {{$chapter->chapter}} {{$chapter->name}} {!!$chapter->readed()!!}</a> <em class="pull-right">{{humanDate($chapter->created_at)}}</em></li>
							@endforeach
						</ul>
					</li>	
				</ul>
				@else
					<div class="text-center text-muted">Tidak ada chapter yang dapat di tampilkan</div>
				@endif
			</div>
		</div>

		<div class="panel panel-default" id="Commentbox">
			<div class="panel-body">
				<div class="fb-comments" migrated="1" notify="true" data-href="{{Request::url()}}" data-numposts="10" data-width="100%" data-order-by="reverse_time"></div>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="panel panel-default">
			<div class="panel-body">
				<img src="{{$manga->getCover()}}" onError="this.src='/img/cover/original/not-available.jpg'" alt="Cover Manga {{$manga->name}}" class="img img-responsive img-thumbnail">
			</div>
			<div class="list-group">
				<div class="list-group-item text-center" id="rating-container">
					<input value="{{$manga->rate}}" type="number" id="rating" class="rating" data-manga="{{$manga->id}}" data-max="5" data-min="1" data-icon-lib="fa fa-2x mr5" data-active-icon="fa-star text-warning" data-inactive-icon="fa-star-o"/>
				</div>
				<div href="javascript:;" class="list-group-item">
					<i class="ti-ink-pen mr10 text-muted"></i>Author : {{$manga->getAuthor->name}}
				</div>
				<div href="javascript:;" class="list-group-item">
					<i class="ti-brush mr10 text-muted"></i>Artist : {{$manga->getArtist->name}}
				</div>
				<div href="javascript:;" class="list-group-item">
					<i class="ti-calendar mr10 text-muted"></i>Released : {{$manga->released}}
				</div>
				<div href="javascript:;" class="list-group-item">
					<i class="ti-tag mr10 text-muted"></i>Genre : 
					@foreach($manga->genre()->get() as $genreRelation)
						<a href="/directory/genre/{{strtolower($genreRelation->genre['name'])}}" class="label label-default">{{$genreRelation->genre['name']}}</a>
					@endforeach
				</div>
				<div href="javascript:;" class="list-group-item">
					<i class="ti-flag mr10 text-muted"></i>Status : <span class="label label-{{$manga->status->color}}">{{$manga->status->name}}</span>
				</div>
			</div>
		</div>
		<div class="mb20">
			<!--  ad tags Size: 300x250 ZoneId:1052368-->
			<script type="text/javascript" src="http://js.adstars.co.id/t/052/368/a1052368.js"></script>
		</div>
	</div>
</div>

<div class="modal fade bs-modal-sm" tabindex="-1" role="dialog" aria-hidden="true" id="suggest-description">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				<h4 class="modal-title">Contact</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12">
						<label>Ringkasan Cerita</label>
						<textarea class="form-control" rows="7"></textarea>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
				<button type="button" class="btn btn-success">Kirim</button>
			</div>
		</div>
	</div>
</div>
@include('_footer')