@include('_header')
<div class="row">
	<div class="col-md-12">
		<!--  ad tags Size: 728x90 ZoneId:1052096-->
		<script type="text/javascript" src="http://js.adstars.co.id/t/052/096/a1052096.js"></script>
	</div>
</div>
<div class="row">
	<div class="col-md-8">
		<div class="panel">
			<div class="panel-body">
				<h4 class="mb20"><b>Directory Manga Halaman {{$mangas->currentPage()}}</b> <b class="pull-right">-Popularitas</b></h4>
				<div class="clearfix"></div>
				<row id="directoryManga">
					@foreach($mangas as $manga)
					<div class="col-md-6 mb15 pl0">
						<div class="mangaCover">
							<img src="{{$manga->getCover('medium')}}" alt="" onError="this.src='/img/cover/original/not-available.jpg'" width="100" height="100" class="img img-thumbnail" alt="Cover Manga {{$manga->name}}">
						</div>
						<div class="mangaDetail">
								<h2 class="mangaTitle"><a href="/manga/{{$manga->slug}}">{{str_limit($manga->name,15)}}</a></h2>
								<p>{{str_limit($manga->getGenre(),20)}}</p>
								<p><input value="{{$manga->rate}}" type="number" readonly="true" disabled="disabled" class="rating" data-manga="{{$manga->id}}" data-max="5" data-min="1" data-icon-lib="fa" data-active-icon="fa-star text-warning" data-inactive-icon="fa-star-o"/></p>
								<p>{{$manga->view}}x Dilihat</p>
								<p><a href="/manga/{{$manga->slug}}/{{$manga->getLastChapter()}}">{{str_limit($manga->name,15)}} {{$manga->getLastChapter()}}</a></p>
							</a>
						</div>
						<div class="clearfix"></div>
					</div>
					@endforeach
				</ul>
				<div class="pull-right">
					{!! $mangas->render() !!}
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		{!!Form::open(array('url'=>'/search/','method'=>'GET'))!!}
		<div class="form-group mb20">
			<div class="input-group">
					<input name="title" placeholder="Ketikan Judul Manga" type="text" class="form-control">

					<div class="input-group-btn">
						<button type="submit" class="btn btn-default"><span class="fa fa-search"></span></button>
					</div>
			</div>
		</div>
		</form>
		<div class="mb20">
			<!--  ad tags Size: 300x250 ZoneId:1052368-->
			<script type="text/javascript" src="http://js.adstars.co.id/t/052/368/a1052368.js"></script>
		</div>
		
		<div class="clearfix"></div>
		<div class="fb-page mb20" data-href="http://facebook.com/manggaku" data-width="301" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="http://facebook.com/manggaku"><a href="http://facebook.com/manggaku">Manggaku - Read Manga Online</a></blockquote></div></div>

		<!-- <div data-manggaku="load" data-target="widget-login"></div> -->
		
		<div class="mb20">
			<!--  ad tags Size: 300x250 ZoneId:1052368-->
			<script type="text/javascript" src="http://js.adstars.co.id/t/052/368/a1052368.js"></script>
		</div>

		<div class="mb20">
			<script src="http://scr.kliksaya.com/js-ad.php?zid=157592" type="text/javascript"></script>
		</div>
		
		<div class="panel">
			<div class="panel-body">
				<h4>
					<b>Browse Manga by Genres</b>
				</h4>
				<ul class="two-column no-liststyle">
					<li>
						<a href="/directory">Semua</a>
					</li>
					@foreach($genres as $genre)
					<li>
						<a href="/directory/genre/{{strtolower($genre->name)}}">{{$genre->name}}</a>
					</li>
					@endforeach
				</ul>
			</div>
		</div>
	</div>
</div>
@include('_footer')