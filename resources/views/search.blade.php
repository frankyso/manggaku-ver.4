@include('_header')
<div class="row">
	<div class="col-md-12">
		<!--  ad tags Size: 728x90 ZoneId:1052096-->
		<script type="text/javascript" src="http://js.adstars.co.id/t/052/096/a1052096.js"></script>
	</div>
</div>
<div class="panel-primary panel">
	<div class="panel-heading">
		Pencarian Manga
	</div>

	<form class="form-horizontal" role="form" action="">
		<div class="panel-body">
			<div class="form-group">
				<label class="col-sm-2 control-label">Judul</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="title" value="<?=$title?>">
				</div>
			</div>
			<!-- <div class="form-group">
				<label class="col-sm-2 control-label">Pengarang</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="author" value="<?=$author?>">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Ilustrasi</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="illustrator" value="<?=$illustrator?>">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Genre</label>
				<div class="col-sm-10">
					<div class="row">
					@foreach ($genresList as $value)
						<div class="checkbox col-md-4 col-sm-6 col-lg-3 col-xs-6">
							<label>
								<input type="checkbox" @if(in_array($value->name,$genre)) checked @endif name="genre[]" value="{{$value->name}}">{{$value->name}}
							</label>
						</div>
					@endforeach
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Rilis</label>
				<div class="col-sm-10">
					<div class="row">
						<div class="col-md-4 col-sm-4 col-xs-4">
							<select name="releaseOrder" class="form-control">
								<option value="1" <?=$releaseOrder==1?"selected":""?>>Tepat</option>
								<option value="2" <?=$releaseOrder==2?"selected":""?>>Diatas</option>
								<option value="3" <?=$releaseOrder==3?"selected":""?>>Dibawah</option>
							</select>
						</div>
						<div class="col-md-8 col-sm-8 col-xs-8">
							<input type="text" class="form-control" placeholder="ex : 2010" name="release" value="{{$release}}">
						</div>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Status</label>
				<div class="col-sm-10">
					<select name="status" class="form-control">
						<option value="">Semua</option>
						<?php foreach ($statusList as $value): ?>
							<option <?=($value->id==$status)?"selected":""?> value="<?=$value->id?>"><?=$value->name?></option>
						<?php endforeach ?>
					</select>
				</div>
			</div> -->
		</div>
		<div class="panel-footer text-right">
			<input class="btn btn-primary" type="submit" value="Lakukan Pencarian">
			<a href="/search" class="btn btn-danger">Reset Filter</a>
		</div>
	</form>
</div>

<div class="panel-primary panel">
	<div class="panel-heading">
		Hasil Pencarian
	</div>
	<table class="table table-bordered table-striped">
		<thead>
			<tr>
				<th width="5%">No.</th>
				<th width="40%">Manga</th>
				<th>Chapter</th>
				<th>Tanggal</th>
				<th>Rating</th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<th width="10%">No.</th>
				<th width="40%">Manga</th>
				<th>Chapter</th>
				<th>Tanggal</th>
				<th>Rating</th>
			</tr>
		</tfoot>
		<tbody>
			<?php $i=1; foreach ($searchResult as $value): ?>
				<tr>
					<td><?=$i?></td>
					<td><a href="/manga/{{$value->slug}}/"><?=$value->name?></a></td>
					<td>Chapter {{$value->getLastChapter()}}</td>
					<td><?=humanDate($value->lastChapter_created_at)?></td>
					<td><input disabled value="<?=$value->rate?>" type="number" class="rating" data-max="5" data-min="1" data-icon-lib="fa mr5" data-active-icon="fa-star text-warning" data-inactive-icon="fa-star-o"/></td>
				</tr>
			<?php $i++; endforeach ?>
		</tbody>
	</table>
</div>
<!-- PopAds.net Popunder Code for manggaku.com -->
<script type="text/javascript">
  var _pop = _pop || [];
  _pop.push(['siteId', 379712]);
  _pop.push(['minBid', 0.000000]);
  _pop.push(['popundersPerIP', 0]);
  _pop.push(['delayBetween', 0]);
  _pop.push(['default', false]);
  _pop.push(['defaultPerDay', 0]);
  _pop.push(['topmostLayer', false]);
  (function() {
    var pa = document.createElement('script'); pa.type = 'text/javascript'; pa.async = true;
    var s = document.getElementsByTagName('script')[0]; 
    pa.src = '//c1.popads.net/pop.js';
    pa.onerror = function() {
      var sa = document.createElement('script'); sa.type = 'text/javascript'; sa.async = true;
      sa.src = '//c2.popads.net/pop.js';
      s.parentNode.insertBefore(sa, s);
    };
    s.parentNode.insertBefore(pa, s);
  })();
</script>
<!-- PopAds.net Popunder Code End -->
@include('_footer')